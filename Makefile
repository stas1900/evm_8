all:compile

compile: test leak las_leak

OBJECT = object
SRC = src/c++
SRC_LASPACK = src/laspack

test: $(OBJECT)/test.o $(OBJECT)/solve.o $(OBJECT)/def.o 
	g++ $(OBJECT)/test.o $(OBJECT)/solve.o $(OBJECT)/def.o -O3 --fast-math -o test;
    
leak: $(OBJECT)/leak.o $(OBJECT)/solve.o $(OBJECT)/def.o 
	g++ $(OBJECT)/leak.o $(OBJECT)/solve.o $(OBJECT)/def.o -O3 --fast-math -o leak;

las_leak:
	gcc $(SRC)/las_leak.cpp $(SRC)/las_solve.cpp $(SRC_LASPACK)/*.c -O3 -lm --fast-math -o las_leak;



$(OBJECT)/solve.o: $(SRC)/solve.cpp $(SRC)/solve.hpp 
	g++ -c $(SRC)/solve.cpp -o $(OBJECT)/solve.o

$(OBJECT)/test.o: $(SRC)/test.cpp $(SRC)/test.hpp 
	g++ -c $(SRC)/test.cpp -o $(OBJECT)/test.o

$(OBJECT)/def.o: $(SRC)/def.cpp $(SRC)/test.hpp $(SRC)/solve.hpp 
	g++ -c $(SRC)/def.cpp -o $(OBJECT)/def.o
    
$(OBJECT)/leak.o: $(SRC)/leak.cpp $(SRC)/solve.hpp 
	g++ -c $(SRC)/leak.cpp -o $(OBJECT)/leak.o
    


clean:
	rm -f $(OBJECT)/*.o test leak las_leak
    
clean_las:
	rm -f las_leak
