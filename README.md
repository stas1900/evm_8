# Какая у меня задача

Моя схема: 16

Область: 13

Алгоритм: CGS.

# Как запускать

Для основных манипуляций с прогой я использовал `jupyter` (сразу не скажу как скачать его).
Очень удобная штука.

Если есть `jupyter`, то можно открыть `manager.ipynb` в `jupyter notebook` - там все есть.

### Отладка

``` 
make test; ./test M1 M2 N T Mu C eps max verbose
```

### Протекание

```
make leak; ./leak M1 M2 tau Mu C eps max verbose omega eps_end max_t
```
но лучше
```
make las_leak; ./las_leak M1 M2 tau Mu C eps max verbose omega eps_end max_t
```

1. `verbose` принимает значения `0 1 2`. При 0 ничего особо не печатается.
При 1 печатается инфа о каждом шаге. При 2 вся эта инфа записывается в `data/query.txt` для 
создания гифки, если это надо.
2. `eps`, `max` - параметры `CGS`. `CGS` останавливается, когда или итераций больше чем `max` или $`||Ax-b||<eps*||b||`$.
3. `omega` это $`\omega`$ в задаче протекания.
4. `eps_end`, `max_t` - параметры при протекании. Если предыдущий слой (плотность и скорости) будут отличаться от текущего на `eps_end`, то заканчиваем. Иначе работаем до `max_t`.


### Картиночки и гифки

Вот usage по картинкам и гифкам:

```python
import src.python.gas2d as asja

asja.print_g(filename = "data/last.txt") # только плотность #jupyter notebook - рисует картинку
asja.print_v2(filename = "data/last.txt") # только v2 скорость #jupyter notebook - рисует картинку
asja.print_gvv(filename = "data/last.txt") # и плотность и скорость #jupyter notebook - рисует картинку

asja.m() # график изменения плотности #jupyter notebook - рисует картинку

asja.gif(fps=100) # гифка #jupyter notebook/python - рисует гифку(в юптере) и создает /data/query.gif
```