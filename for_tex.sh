#!/bin/sh

for Mu in 0.1 0.01 0.001; 
    do for C in 1 10; 
        do for M in 31 61 121 241 481; 
            do for N in 20 40 80 160; 
                do  echo Mu=$Mu C=$C M=$M N=$N;
                    ./test $M $M $N 1 $Mu $C 1e-8 2000 0; 
            done; 
        done; 
    done; 
done;