#include"solve.hpp"
#include"test.hpp"


int toInt(const char *s,int *xp){
    long l;
    char *e;
    errno=0;
    l=strtol(s,&e,10);
    if(!errno && *e=='\0'){
        if(INT_MIN<=l && l<=INT_MAX){
            *xp=(int)l;
            return 0;}
        else
            return -1;}
return -1;}

int toDouble(const char *s,double *xp){
    double l;
    char *e;
    errno=0;
    l=strtod(s,&e);
    if(!errno && *e=='\0'){
        if(-DBL_MAX<=l && l<=DBL_MAX){
            *xp=(double)l;
            return 0;}
        else
            return -1;}
return -1;}


double p_true(double x1, double x2, double t)
{
    (void)(x1*x2*t);
    return P_TRUE;
}

double g_true(double x1, double x2, double t)
{
    (void)(x1*x2*t);
    return G_TRUE;
}

double v1_true(double x1, double x2, double t)
{
    (void)(x1*x2*t);
    return V1_TRUE;
}

double v2_true(double x1, double x2, double t)
{
    (void)(x1*x2*t);
    return V2_TRUE;
}

double p_initial(double x1, double x2)
{
    (void)(x1*x2);
    return P_INITIAL;
}

double g_initial(double x1, double x2)
{
    (void)(x1*x2);
    return G_INITIAL;
}

double v1_initial(double x1, double x2, double omega)
{
    (void)(x1*x2*omega);
    return V1_INITIAL;
}

double v2_initial(double x1, double x2)
{
    (void)(x1*x2);
    return V2_INITIAL;
}

double p_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return RHO_0;
}

double g_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return log(RHO_0);
}

double v1_initial_leak(double x1, double x2, double omega)
{
    (void)(x1*x2);
    if(x1 < 1e-8)
        return omega;
    return 0;
}

double v2_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return 0;
}

double f0(double x1, double x2, double t, double C, double Mu)
{ 
    (void)(x1*x2*t*C*Mu);
    return FUNCTION_0;
}

double f1(double x1, double x2, double t, double C, double Mu)
{ 
    (void)(x1*x2*t*C*Mu);
    return FUNCTION_1;
}

double f2(double x1, double x2, double t, double C, double Mu)
{ 
    (void)(x1*x2*t*C*Mu);
    return FUNCTION_2;
}

void init(Node* Nodes, Args arg, Initial initial)
{         
    // g v1 v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.m1; j++)
        {
            int ind = i*arg.m1 + j;
            
            Nodes[ind].x = arg.X1/3 + j*arg.h1;
            Nodes[ind].y = i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m1;
            Nodes[ind].down = ind - arg.m1;
            Nodes[ind].status = 0;
        }
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
        {
            int ind = arg.size1 + i*arg.m2 + j;
            
            Nodes[ind].x = j*arg.h1;
            Nodes[ind].y = arg.X2/3 + i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m2;
            Nodes[ind].down = ind - arg.m2;
            Nodes[ind].status = 0;
        }
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.m3; j++)
        {
            int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
            
            Nodes[ind].x = 2*arg.X1/3 + j*arg.h1;
            Nodes[ind].y = 2*arg.X2/3 + arg.h2 + i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m3;
            Nodes[ind].down = ind - arg.m3;
            Nodes[ind].status = 0;
        }
    }
    
    
    //up down
    
    //Omega_1
    
    for(int j = 0; j < arg.m1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].down = -1;
    }
    
    for(int j = 0; j < arg.m1; j++)
    {
        int i = arg.n1 - 1;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].up = arg.size1 + (arg.M1-1)/3 + j;
    }
   

    //Omega_2
    
    for(int j = 0; j < arg.m2; j++)
    {
        int i = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        if(j < (arg.M1 - 1)/3)
            Nodes[ind].down = -1;
        else
            Nodes[ind].down = (arg.n1-1)*arg.m1 + j - (arg.M1 - 1)/3;
    }
    
    for(int j = 0; j < arg.m2; j++)
    {
        int i =  arg.n2 - 1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        if(j < (2*arg.M1-2)/3)
            Nodes[ind].up = -1;
        else
            Nodes[ind].up = arg.size1 + arg.size2 + j - (2*arg.M1-2)/3;
    }
    
    //Omega_3
    
    for(int j = 0; j < arg.m3; j++)
    {
        int i = 0;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].down = arg.size1 + (arg.n2-1)*arg.m2 + j + (2*arg.M1-2)/3;
    }
    
    for(int j = 0; j < arg.m3; j++)
    {
        int i = arg.n3 - 1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].up = -1;
    }
    
    
    //status
    
    //Omega_1
    
    
    for(int j = 1; j < arg.m1 - 1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 3;
    }
    
    Nodes[0].status = 5;
    Nodes[arg.m1 - 1].status = 6;
    
    for(int i = 1; i < arg.n1; i++)
    {
        int j = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 1;
    }
    
    for(int i = 1; i < arg.n1; i++)
    {
        int j = arg.m1 - 1;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 2;
    }
   

    //Omega_2
    
    
    for(int j = 1; j < (arg.M1 + 2)/3 - 1; j++)
    {
        int i = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 3;
    }
    
    Nodes[arg.size1].status = 5;
    Nodes[arg.size1 + (arg.M1 + 2)/3 - 1].status = 3; //10
    
    
    
    for(int i = 1; i < arg.n2 - 1; i++)
    {
        int j = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 1;
    }
    
    for(int i = 0; i < arg.n2; i++)
    {
        int j = arg.m2-1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 2;
    }
    
    for(int j = 1; j < (2*arg.M1 + 1)/3 - 1; j++)
    {
        int i = arg.n2-1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 4;
    }
    
    Nodes[arg.size1 + (arg.n2-1)*arg.m2].status = 7;
    Nodes[arg.size1 + (arg.n2-1)*arg.m2 + (2*arg.M1 + 1)/3 - 1].status = 4; //11
    
    //Omega_3
    

    for(int i = 0; i < arg.n3 - 1; i++)
    {
        int j = 0;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 1;
    }
    
    Nodes[arg.size1 + arg.size2 + (arg.n3 - 1)*arg.m3].status = 7;
    
    for(int j = 1; j < arg.m3 - 1; j++)
    {
        int i = arg.n3-1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 4;
    }
    
    Nodes[arg.size1 + arg.size2 + (arg.n3 - 1)*arg.m3 + arg.m3 - 1].status = 8;
    
    for(int i = 0; i < arg.n3 - 1; i++)
    {
        int j = arg.m3 - 1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 2;
    }
}

void ext_status(Node *Nodes, Args arg)
{         
    //status
    
    //Omega_1
    
    
    for(int j = 1; j < arg.m1-1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 12;
    }

    //Omega_2
    
    for(int i = 0; i < arg.n2; i++)
    {
        int j = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 13;
    }
    
    //Omega_3
    
    for(int j = 1; j < arg.m3-1; j++)
    {
        int i = arg.n3-1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 14;
    }
}

void real(Node *Nodes, Args arg)
{         
    // g v1 v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.m1; j++)
        {
            int ind = i*arg.m1 + j;
            
            Nodes[ind].x = arg.X1/3 + j*arg.h1;
            Nodes[ind].y = i*arg.h2;
            
            Nodes[ind].g  =  g_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v1 = v1_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v2 = v2_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            
        }
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
        {
            int ind = arg.size1 + i*arg.m2 + j;
            
            Nodes[ind].x = j*arg.h1;
            Nodes[ind].y = arg.X2/3 + i*arg.h2;
            
            Nodes[ind].g  =  g_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v1 = v1_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v2 = v2_true(Nodes[ind].x, Nodes[ind].y, arg.T);
        }
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.m3; j++)
        {
            int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
            
            Nodes[ind].x = 2*arg.X1/3 + j*arg.h1;
            Nodes[ind].y = 2*arg.X2/3 + arg.h2 + i*arg.h2;
            
            Nodes[ind].g  =  g_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v1 = v1_true(Nodes[ind].x, Nodes[ind].y, arg.T);
            Nodes[ind].v2 = v2_true(Nodes[ind].x, Nodes[ind].y, arg.T);
        }
    }
}

int save_gvv_txt(const char* filename, Node* Nodes, Args arg, double t, double m)
{    
    FILE* file = fopen(filename, "w+");
    if(!file)
        return 0;

    fprintf(file, "%d %d t = %g m = %g\n",arg.M1, arg.M2, t, m);

    //g
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].g);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].g);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].g);

        fprintf(file, "\n");
    }
    
    
    //v1
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].v1);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].v1);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].v1);

        fprintf(file, "\n");
    }
    
    
    //v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].v2);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].v2);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].v2);

        fprintf(file, "\n");
    }

    fclose(file);

    return 1;

}


void print(Node* Nodes, Args arg)
{    
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(stdout, "-2 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(stdout, "%-2d ", Nodes[i*arg.m1 + j].status);

        fprintf(stdout, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(stdout, "%-2d ", Nodes[arg.size1 + i*arg.m2 + j].status);

        fprintf(stdout, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(stdout, "-2 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(stdout, "%-2d ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].status);

        fprintf(stdout, "\n");
    }
}



int save_gvv_txt_plus(const char* filename, Node* Nodes, Args arg, double t, double m)
{    
    FILE* file = fopen(filename, "ab");
    if(!file)
        return 0;
    
    fseek(file, 0L, SEEK_END);

    fprintf(file, "%d %d t = %g m = %g\n",arg.M1, arg.M2, t, m);

    //g
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].g);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].g);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].g);

        fprintf(file, "\n");
    }
    
    
    //v1
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].v1);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].v1);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].v1);

        fprintf(file, "\n");
    }
    
    
    //v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", Nodes[i*arg.m1 + j].v2);

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + i*arg.m2 + j].v2);

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", Nodes[arg.size1 + arg.size2 + i*arg.m3 + j].v2);

        fprintf(file, "\n");
    }

    fclose(file);

    return 1;

}

int save_m_txt_plus(const char* filename, double t, double m)
{    
    FILE* file = fopen(filename, "ab");
    if(!file)
        return 0;
    
    fseek(file, 0L, SEEK_END);

    fprintf(file, "t = %g m = %g\n",t, m);
    fclose(file);

    return 1;
}