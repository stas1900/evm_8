#include"solve.hpp"

#include "../laspack/qmatrix.h"
#include "../laspack/rtc.h"
#include "../laspack/errhandl.h"
#include "../laspack/vector.h"

void las_leak_init(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);

void calculate_las_leak(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up, double *t_end);

int save_gvv_las_txt(const char* filename, Vector * x_g, Vector * x_v1, Vector * x_v2, Args arg, double t, double m);

int main(int argc,char *argv[])
{
    int M1, M2, iter_max, verbose;
    double Mu, C, tau, eps, omega, eps_end, max_t, t_end, m;

    if(argc!=12){
        printf("USAGE: %s <M1> <M2> <tau> <Mu> <C> <eps> <max> <verbose> <omega> <eps_end> <max_t> \n",argv[0]);
    return -1;}

    
    // Моя область:
    // - - +
    // + + +
    // - + +
    
    if(argc == 12)
    {
        if(toInt(argv[1], &M1)!=0       || 
           toInt(argv[2], &M2)!=0       || 
           toDouble(argv[3], &tau)!=0     ||
           toDouble(argv[4], &Mu)!=0    ||
           toDouble(argv[5], &C)!=0     || 
           toDouble(argv[6], &eps)!=0   ||
           toInt(argv[7], &iter_max)!=0 ||
           toInt(argv[8], &verbose)!=0  ||
           toDouble(argv[9], &omega)!=0 ||
           toDouble(argv[10], &eps_end)!=0 || 
           toDouble(argv[11], &max_t)!=0
          )
            {
                printf("Incorrect input data\n");
                return -1;
            }

        if(M1%3!=1 || M2%3!=1 || M1==1 || M2==1)
        {
            printf("M1%3!=1 or M2!%3!=1\n");
            return -2; 
        }
        
        double X1 = 3;
        double X2 = 3;
        double t_end;
        Args arg = Args(M1, M2, tau, X1, X2, Mu, C, verbose, omega, eps_end, max_t, eps, iter_max);
        
        int * up = (int *)malloc((arg.size+1)*sizeof(int));
        int * down = (int *)malloc((arg.size+1)*sizeof(int));
        int * status = (int *)malloc((arg.size+1)*sizeof(int));
        
        if(arg.verbose == 2)
        {
            fopen("data/query.txt", "w+");
        }
        fopen("data/m.txt", "w+");
        
        //START LASPACK
        
        QMatrix_L A_g, A_v1, A_v2;
        Vector b_g, b_v1, b_v2, x_g, x_v1, x_v2;
        

        las_leak_init(&A_g, &A_v1, &A_v2, &x_g, &x_v1, &x_v2, &b_g, &b_v1, &b_v2, arg, status, down, up);
        calculate_las_leak(&A_g, &A_v1, &A_v2, &x_g, &x_v1, &x_v2, &b_g, &b_v1, &b_v2, arg, status, down, up, &t_end);
        
        double m = 0.;
        for(int i = 1; i <= arg.size; i++)
            m += arg.h1*arg.h2*exp(V_GetCmp(&x_g, i));
        if(!save_gvv_las_txt("data/last.txt", &x_g, &x_v1, &x_v2, arg, t_end, m))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        Q_Destr(&A_g);
        Q_Destr(&A_v1);
        Q_Destr(&A_v2);
        V_Destr(&b_g);
        V_Destr(&b_v1);
        V_Destr(&b_v2);
        
        //END LASPACK
        free(up);
        free(down);
        free(status);
        


    }
    return 0;
}










