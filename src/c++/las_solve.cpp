#include "solve.hpp"

#include "../laspack/qmatrix.h"
#include "../laspack/rtc.h"
#include "../laspack/errhandl.h"
#include "../laspack/vector.h"
#include "../laspack/itersolv.h"



int toInt(const char *s,int *xp){
    long l;
    char *e;
    errno=0;
    l=strtol(s,&e,10);
    if(!errno && *e=='\0'){
        if(INT_MIN<=l && l<=INT_MAX){
            *xp=(int)l;
            return 0;}
        else
            return -1;}
return -1;}

int toDouble(const char *s,double *xp){
    double l;
    char *e;
    errno=0;
    l=strtod(s,&e);
    if(!errno && *e=='\0'){
        if(-DBL_MAX<=l && l<=DBL_MAX){
            *xp=(double)l;
            return 0;}
        else
            return -1;}
return -1;}

int save_m_txt_plus(const char* filename, double t, double m)
{    
    FILE* file = fopen(filename, "ab");
    if(!file)
        return 0;
    
    fseek(file, 0L, SEEK_END);

    fprintf(file, "t = %g m = %g\n",t, m);
    fclose(file);

    return 1;
}


int save_gvv_las_txt(const char* filename, Vector * x_g, Vector * x_v1, Vector * x_v2, Args arg, double t, double m)
{    
    FILE* file = fopen(filename, "w+");
    if(!file)
        return 0;

    fprintf(file, "%d %d t = %g m = %g\n",arg.M1, arg.M2, t, m);

    //g
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }
    
    
    //v1
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }
    
    
    //v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }

    fclose(file);

    return 1;

}

int save_gvv_las_txt_plus(const char* filename, Vector * x_g, Vector * x_v1, Vector * x_v2, Args arg, double t, double m)
{    
    FILE* file = fopen(filename, "ab");
    if(!file)
        return 0;
    
    fseek(file, 0L, SEEK_END);

    fprintf(file, "%d %d t = %g m = %g\n",arg.M1, arg.M2, t, m);

    //g
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_g, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }
    
    
    //v1
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v1, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }
    
    
    //v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m1; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m1; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, i*arg.m1 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, arg.size1 + i*arg.m2 + j + 1));

        fprintf(file, "\n");
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.M1 - arg.m3; j++)
            fprintf(file, "-1 ");

        for(int j = 0; j < arg.m3; j++)
            fprintf(file, "%lf ", V_GetCmp(x_v2, arg.size1 + arg.size2 + i*arg.m3 + j + 1));

        fprintf(file, "\n");
    }

    fclose(file);

    return 1;

}


double p_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return RHO_0;
}

double g_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return log(RHO_0);
}

double v1_initial_leak(double x1, double x2, double omega)
{
    (void)(x1*x2);
    if(x1 < 1e-8)
        return omega;
    return 0;
}

double v2_initial_leak(double x1, double x2)
{
    (void)(x1*x2);
    return 0;
}

void init(Node* Nodes, Args arg, Initial initial)
{         
    // g v1 v2
    //Omega_1
    for(int i = 0; i < arg.n1; i++)
    {
        for(int j = 0; j < arg.m1; j++)
        {
            int ind = i*arg.m1 + j;
            
            Nodes[ind].x = arg.X1/3 + j*arg.h1;
            Nodes[ind].y = i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m1;
            Nodes[ind].down = ind - arg.m1;
            Nodes[ind].status = 0;
        }
    }

    //Omega_2
    for(int i = 0; i < arg.n2; i++)
    {
        for(int j = 0; j < arg.m2; j++)
        {
            int ind = arg.size1 + i*arg.m2 + j;
            
            Nodes[ind].x = j*arg.h1;
            Nodes[ind].y = arg.X2/3 + i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m2;
            Nodes[ind].down = ind - arg.m2;
            Nodes[ind].status = 0;
        }
    }

    //Omega_3
    for(int i = 0; i < arg.n3; i++)
    {
        for(int j = 0; j < arg.m3; j++)
        {
            int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
            
            Nodes[ind].x = 2*arg.X1/3 + j*arg.h1;
            Nodes[ind].y = 2*arg.X2/3 + arg.h2 + i*arg.h2;
            
            Nodes[ind].g  = initial.g(Nodes[ind].x, Nodes[ind].y);
            Nodes[ind].v1 = initial.v1(Nodes[ind].x, Nodes[ind].y, arg.omega);
            Nodes[ind].v2 = initial.v2(Nodes[ind].x, Nodes[ind].y);
            
            Nodes[ind].up = ind + arg.m3;
            Nodes[ind].down = ind - arg.m3;
            Nodes[ind].status = 0;
        }
    }
    
    
    //up down
    
    //Omega_1
    
    for(int j = 0; j < arg.m1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].down = -1;
    }
    
    for(int j = 0; j < arg.m1; j++)
    {
        int i = arg.n1 - 1;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].up = arg.size1 + (arg.M1-1)/3 + j;
    }
   

    //Omega_2
    
    for(int j = 0; j < arg.m2; j++)
    {
        int i = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        if(j < (arg.M1 - 1)/3)
            Nodes[ind].down = -1;
        else
            Nodes[ind].down = (arg.n1-1)*arg.m1 + j - (arg.M1 - 1)/3;
    }
    
    for(int j = 0; j < arg.m2; j++)
    {
        int i =  arg.n2 - 1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        if(j < (2*arg.M1-2)/3)
            Nodes[ind].up = -1;
        else
            Nodes[ind].up = arg.size1 + arg.size2 + j - (2*arg.M1-2)/3;
    }
    
    //Omega_3
    
    for(int j = 0; j < arg.m3; j++)
    {
        int i = 0;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].down = arg.size1 + (arg.n2-1)*arg.m2 + j + (2*arg.M1-2)/3;
    }
    
    for(int j = 0; j < arg.m3; j++)
    {
        int i = arg.n3 - 1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].up = -1;
    }
    
    
    //status
    
    //Omega_1
    
    
    for(int j = 1; j < arg.m1 - 1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 3;
    }
    
    Nodes[0].status = 5;
    Nodes[arg.m1 - 1].status = 6;
    
    for(int i = 1; i < arg.n1; i++)
    {
        int j = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 1;
    }
    
    for(int i = 1; i < arg.n1; i++)
    {
        int j = arg.m1 - 1;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 2;
    }
   

    //Omega_2
    
    
    for(int j = 1; j < (arg.M1 + 2)/3 - 1; j++)
    {
        int i = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 3;
    }
    
    Nodes[arg.size1].status = 5;
    Nodes[arg.size1 + (arg.M1 + 2)/3 - 1].status = 3; //10
    
    
    
    for(int i = 1; i < arg.n2 - 1; i++)
    {
        int j = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 1;
    }
    
    for(int i = 0; i < arg.n2; i++)
    {
        int j = arg.m2-1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 2;
    }
    
    for(int j = 1; j < (2*arg.M1 + 1)/3 - 1; j++)
    {
        int i = arg.n2-1;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 4;
    }
    
    Nodes[arg.size1 + (arg.n2-1)*arg.m2].status = 7;
    Nodes[arg.size1 + (arg.n2-1)*arg.m2 + (2*arg.M1 + 1)/3 - 1].status = 4; //11
    
    //Omega_3
    

    for(int i = 0; i < arg.n3 - 1; i++)
    {
        int j = 0;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 1;
    }
    
    Nodes[arg.size1 + arg.size2 + (arg.n3 - 1)*arg.m3].status = 7;
    
    for(int j = 1; j < arg.m3 - 1; j++)
    {
        int i = arg.n3-1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 4;
    }
    
    Nodes[arg.size1 + arg.size2 + (arg.n3 - 1)*arg.m3 + arg.m3 - 1].status = 8;
    
    for(int i = 0; i < arg.n3 - 1; i++)
    {
        int j = arg.m3 - 1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 2;
    }
}

void ext_status(Node *Nodes, Args arg)
{         
    //status
    
    //Omega_1
    
    
    for(int j = 1; j < arg.m1-1; j++)
    {
        int i = 0;
        int ind = i*arg.m1 + j;
        
        Nodes[ind].status = 12;
    }

    //Omega_2
    
    for(int i = 0; i < arg.n2; i++)
    {
        int j = 0;
        int ind = arg.size1 + i*arg.m2 + j;
        
        Nodes[ind].status = 13;
    }
    
    //Omega_3
    
    for(int j = 1; j < arg.m3-1; j++)
    {
        int i = arg.n3-1;
        int ind = arg.size1 + arg.size2 + i*arg.m3 + j;
        
        Nodes[ind].status = 14;
    }
}

void A_g_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);
void A_v1_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);
void A_v2_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);

void b_g_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);
void b_v1_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);
void b_v2_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);

void las_leak_next(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up);

void las_leak_init(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{
    Initial initial;
    initial.p = p_initial_leak;
    initial.g = g_initial_leak;
    initial.v1 = v1_initial_leak;
    initial.v2 = v2_initial_leak;
    
    Node * Nodes = (Node *)malloc(arg.size*sizeof(Node)); 
    
    init(Nodes, arg, initial);
    ext_status(Nodes, arg);

    Q_Constr (A_g, "A_g", arg.size, False, Rowws, Normal, True);
    V_Constr (b_g, "b_g", arg.size, Normal, True);
    V_Constr (x_g, "x_g", arg.size, Normal, True);

    Q_Constr (A_v1, "A_v1", arg.size, False, Rowws, Normal, True);
    V_Constr (b_v1, "b_v1", arg.size, Normal, True);
    V_Constr (x_v1, "x_v1", arg.size, Normal, True);

    Q_Constr (A_v2, "A_v2", arg.size, False, Rowws, Normal, True);
    V_Constr (b_v2, "b_v2", arg.size, Normal, True);
    V_Constr (x_v2, "x_v2", arg.size, Normal, True);
    
    for(int i = 1; i <= arg.size; i++)
    {
        //set initial A
        up[i] = Nodes[i-1].up+1;
        down[i] = Nodes[i-1].down+1;
        status[i] = Nodes[i-1].status;
        switch(status[i])
        {
            case 0:
                Q_SetLen(A_g, i, 5);
                Q_SetLen(A_v1, i, 5);
                Q_SetLen(A_v2, i, 5);     
            break;
            case 12:
                Q_SetLen(A_g, i, 2);
                Q_SetLen(A_v1, i, 1);
                Q_SetLen(A_v2, i, 2);  
            case 13:
                Q_SetLen(A_g, i, 1);
                Q_SetLen(A_v1, i, 1);
                Q_SetLen(A_v2, i, 1);
            case 14:
                Q_SetLen(A_g, i, 2);
                Q_SetLen(A_v1, i, 1);
                Q_SetLen(A_v2, i, 2);
            break;
            default:
                Q_SetLen(A_g, i, 2);
                Q_SetLen(A_v1, i, 1);
                Q_SetLen(A_v2, i, 1);
            break;
        }
        
        //set initial x
        V_SetCmp(x_g, i, Nodes[i-1].g);
        V_SetCmp(x_v1, i, Nodes[i-1].v1);
        V_SetCmp(x_v2, i, Nodes[i-1].v2);
        
        //V_SetCmp(x_v2, i, 10.);
        //printf("%f\n", V_GetCmp(x_v1, i));
    }
    
    //set accuracy of ||Ax-b|| < eps*||b||
    SetRTCAccuracy(arg.eps);
   
    
    free(Nodes);
}

void calculate_las_leak(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up, double *t_end)
{
    double S1, S2, a, b, res;
    CGS_res warn_g, warn_v1, warn_v2;
    double tau = arg.tau;
    
    double m_prev = 0., m_cur = 0.;
    for(int i = 1; i <= arg.size; i++)
        m_prev += arg.h1*arg.h2*exp(V_GetCmp(x_g, i));
    
    double g_eps, v1_eps, v2_eps, res_g, res_v1, res_v2;
    int g_it, v1_it, v2_it;
    int total_iter_g = 0, total_iter_v1 = 0, total_iter_v2 = 0;
    double t;
    
    double *temp = (double*)malloc((arg.size+1)*sizeof(double));
    
    if(arg.verbose == 2)
        save_gvv_las_txt_plus("data/query.txt", x_g, x_v1, x_v2, arg, 0., m_prev);
    save_m_txt_plus("data/m.txt", 0., m_prev);
    
    clock_t timer = clock();
    for(int s=1; !(s*arg.tau > arg.max_t); s++)
    {
        t = s*arg.tau;
        if(arg.verbose != 0)
            printf("\n t = %-10g   prev_res ::::", t);
        

        las_leak_next(A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        /*printf("\n");
        for(int i = 1; i <= arg.size; i++)
            printf("%d:%f ", i, V_GetCmp(b_v1, i));
        printf("\n");*/
        
        
        for(int i = 1; i <= arg.size; i++)
            temp[i] = V_GetCmp(x_g, i);
        
        /*for(int i = 1; i <= arg.size; i++)
        {
            V_SetCmp(x_g, i, 0.);
            V_SetCmp(x_v1, i, 0.);
            V_SetCmp(x_v2, i, 0.);
        }*/
        

        //printf("%d\n", arg.itermax);
        CGSIter(A_g, x_g, b_g, arg.itermax, NULL, 1, &g_eps, &g_it);
        //return;
        res_g = 0.;
        for(int i = 1; i <= arg.size; i++)
        {
            if(res_g < fabs(V_GetCmp(x_g, i) - temp[i]))
                res_g = fabs(V_GetCmp(x_g, i) - temp[i]);
        }
        
        
        for(int i = 1; i <= arg.size; i++)
            temp[i] = V_GetCmp(x_v1, i);
        CGSIter(A_v1, x_v1, b_v1, arg.itermax, NULL, 1, &v1_eps, &v1_it);
        res_v1 = 0.;
        for(int i = 1; i <= arg.size; i++)
        {
            if(res_v1 < fabs(V_GetCmp(x_v1, i) - temp[i]))
                res_v1 = fabs(V_GetCmp(x_v1, i) - temp[i]);
        }
        
        
        for(int i = 1; i <= arg.size; i++)
            temp[i] = V_GetCmp(x_v2, i);
        CGSIter(A_v2, x_v2, b_v2, arg.itermax, NULL, 1, &v2_eps, &v2_it);
        res_v2 = 0.;
        for(int i = 1; i <= arg.size; i++)
        {
            if(res_v2 < fabs(V_GetCmp(x_v2, i) - temp[i]))
                res_v2 = fabs(V_GetCmp(x_v2, i) - temp[i]);
        }
        
        if(arg.verbose != 0)
        {
            printf("     %e     " , res_g);
            printf("     %e     " , res_v1);
            printf("     %e     " , res_v2);
            //printf("\n");
            
        }
        m_cur = 0.;
        for(int i = 1; i <= arg.size; i++)
            m_cur += arg.h1*arg.h2*exp(V_GetCmp(x_g, i));
        
        if(arg.verbose != 0)
        {
            printf("\n dm = %e CGS_res ::::", m_cur - m_prev);
            printf("     %e it=%d" , g_eps, g_it);
            printf("     %e it=%d", v1_eps, v1_it);
            printf("     %e it=%d", v2_eps, v2_it);
            total_iter_g  += g_it;
            total_iter_v1 += v1_it;
            total_iter_v2 += v2_it;
            printf("\n=================================================================================================\n");
        }
        
        m_prev= m_cur;
            if(arg.verbose == 2)
        save_gvv_las_txt_plus("data/query.txt", x_g, x_v1, x_v2, arg, t, m_prev);
        save_m_txt_plus("data/m.txt",  s*arg.tau, m_prev);
        
        if(res_g < arg.eps_end && res_v1 < arg.eps_end && res_v2 < arg.eps_end)
            break;
        
    }
    timer = clock() - timer;
    if(arg.verbose != 0)
        printf("elapsed work CGS: %f with %d + %d + %d = %d iter. So %e iter per sec\n", ((float)timer)/CLOCKS_PER_SEC, total_iter_g, total_iter_v1, total_iter_v2, total_iter_g + total_iter_v1 + total_iter_v2,  (total_iter_g + total_iter_v1 + total_iter_v2)/(((float)timer)/CLOCKS_PER_SEC));
   *t_end = t;
    free(temp);
}

void las_leak_next(QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{
    
    for(int i = 1; i <= arg.size; i++)
    {
        //set new b
        b_g_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        //printf("%f\n", V_GetCmp(x_v1, i));
        b_v1_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        b_v2_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        
        
        //set new row of A
        A_g_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        A_v1_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
        A_v2_las_leak_next(i, A_g, A_v1, A_v2, x_g, x_v1, x_v2, b_g, b_v1, b_v2, arg, status, down, up);
    }
        
}


//A
void A_v1_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    if(status[i] == 0)
    {
        res = (-exp(V_GetCmp(x_g, i))*V_GetCmp(x_v1, i) - exp(V_GetCmp(x_g, i-1))*V_GetCmp(x_v1, i-1) )/(6*h1) - 4./3.*arg.Mu/(h1*h1);
        Q_SetEntry(A_v1, i, 0, i-1, res);
        
        res = exp(V_GetCmp(x_g, i))/tau + 8./3.*arg.Mu/(h1*h1) + 2*arg.Mu/(h2*h2);
        Q_SetEntry(A_v1, i, 1, i, res);
        
        res = ( exp(V_GetCmp(x_g, i))*V_GetCmp(x_v1, i) + exp(V_GetCmp(x_g, i+1))*V_GetCmp(x_v1, i+1) )/(6*h1) - 4./3.*arg.Mu/(h1*h1);
        Q_SetEntry(A_v1, i, 2, i+1, res);
        
        res = (-exp(V_GetCmp(x_g, i))*V_GetCmp(x_v2, i) -exp(V_GetCmp(x_g, down[i]))*V_GetCmp(x_v2, down[i]))/(4*h2) - arg.Mu/(h2*h2);
        Q_SetEntry(A_v1, i, 3, down[i], res);
        
        res = ( exp(V_GetCmp(x_g, i))*V_GetCmp(x_v2, i) + exp(V_GetCmp(x_g, up[i]))*V_GetCmp(x_v2, up[i]) )/(4*h2) - arg.Mu/(h2*h2);
        Q_SetEntry(A_v1, i, 4, up[i], res);
    }
    else
    {
        res = 1.;
        Q_SetEntry(A_v1, i, 0, i, res);
    }
}

void A_v2_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    if(status[i] == 0)
    {            
        res = (-exp(V_GetCmp(x_g, i))*V_GetCmp(x_v1, i) - exp(V_GetCmp(x_g, i-1))*V_GetCmp(x_v1, i-1) )/(4*h1) - arg.Mu/(h1*h1);
        Q_SetEntry(A_v2, i, 0, i-1, res);
        
        res = exp(V_GetCmp(x_g, i))/tau + 8./3.*arg.Mu/(h2*h2) + 2*arg.Mu/(h1*h1);
        Q_SetEntry(A_v2, i, 1, i, res);
        
        res = ( exp(V_GetCmp(x_g, i))*V_GetCmp(x_v1, i) + exp(V_GetCmp(x_g, i+1))*V_GetCmp(x_v1, i+1) )/(4*h1) - arg.Mu/(h1*h1);
        Q_SetEntry(A_v2, i, 2, i+1, res);
        
        res = (-exp(V_GetCmp(x_g, i))*V_GetCmp(x_v2, i) -exp(V_GetCmp(x_g, down[i]))*V_GetCmp(x_v2, down[i]))/(6*h2) - 4./3.*arg.Mu/(h2*h2);
        Q_SetEntry(A_v2, i, 3, down[i], res);
        
        res = ( exp(V_GetCmp(x_g, i))*V_GetCmp(x_v2, i) + exp(V_GetCmp(x_g, up[i]))*V_GetCmp(x_v2, up[i]) )/(6*h2) - 4./3.*arg.Mu/(h2*h2);
        Q_SetEntry(A_v2, i, 4, up[i], res);
    }
    else
    {
        if(status[i] == 12)
        {
            res = 1./h2;
            Q_SetEntry(A_v2, i, 0, up[i], res);
            
            res = -1./h2;
            Q_SetEntry(A_v2, i, 1, i, res);
        }
        else
        {
            if(status[i] == 14)
            {
                res = 1./h2;
                Q_SetEntry(A_v2, i, 0, i, res);

                res = -1./h2;
                Q_SetEntry(A_v2, i, 1, down[i], res);   
            }
            else
            {
                res = 1.;
                Q_SetEntry(A_v2, i, 0, i, res);
            }
        }
    }
}

void A_g_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    switch ( status[i] ) 
    {
        case 0:
            res =  (V_GetCmp(x_v2, i) + V_GetCmp(x_v2, up[i])) /(4*h2);
            Q_SetEntry(A_g, i, 0, up[i], res);
            
            res =  (-V_GetCmp(x_v2, i) - V_GetCmp(x_v2, down[i])) /(4*h2);
            Q_SetEntry(A_g, i, 1, down[i], res);
            
            res =  ( 1./tau );
            Q_SetEntry(A_g, i, 2, i, res);
            
            res =  (-V_GetCmp(x_v1, i) - V_GetCmp(x_v1, i-1)) /(4*h1);
            Q_SetEntry(A_g, i, 3, i-1, res);
            
            res =  (V_GetCmp(x_v1, i) + V_GetCmp(x_v1, i+1)) /(4*h1);
            Q_SetEntry(A_g, i, 4, i+1, res);
        break;

        case 1: //x-
            res =  (1./tau - V_GetCmp(x_v1, i)/(2*h1));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v1, i+1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i+1, res);
        break;

        case 3: //y-
            res =  (1./tau - V_GetCmp(x_v2, i)/(2*h2));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v2, up[i])/(2*h2));
            Q_SetEntry(A_g, i, 1, up[i], res);
        break;

        case 2: //x+
            res =  (1./tau + V_GetCmp(x_v1, i)/(2*h1)); //
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  (-V_GetCmp(x_v1, i-1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i-1, res);
        break;

        case 4: //y+
            res =  (1./tau + V_GetCmp(x_v2, i)/(2*h2)); //
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  (-V_GetCmp(x_v2, down[i])/(2*h2));
            Q_SetEntry(A_g, i, 1, down[i], res);
        break;


        case 5: //x- y-
            res =  (1./tau - V_GetCmp(x_v1, i)/(2*h1));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v1, i+1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i+1, res);
        break;

        case 7: //x- y+
            res =  (1./tau - V_GetCmp(x_v1, i)/(2*h1));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v1, i+1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i+1, res);
        break;


        case 6: //x+ y-
            res =  (1./tau + V_GetCmp(x_v1, i)/(2*h1)); //
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  (-V_GetCmp(x_v1, i-1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i-1, res);
        break;

        case 8: //x+ y+
            res =  (1./tau + V_GetCmp(x_v1, i)/(2*h1)); //
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  (-V_GetCmp(x_v1, i-1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i-1, res);
        break;


        ///////////
        case 10:
            res =  (1./tau - V_GetCmp(x_v1, i)/(2*h1));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v1, i+1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i+1, res);
        break;

        case 11:
            res =  (1./tau - V_GetCmp(x_v1, i)/(2*h1));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v1, i+1)/(2*h1));
            Q_SetEntry(A_g, i, 1, i+1, res);
        break;

        ///////////
        case 12: //y-
            res =  (1./tau - V_GetCmp(x_v2, i)/(2*h2));
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  ( V_GetCmp(x_v2, up[i])/(2*h2));
            Q_SetEntry(A_g, i, 1, up[i], res);
        break;

        case 13: //x-
            res =  1.;
            Q_SetEntry(A_g, i, 0, i, res);
            
        break;

        case 14: //y+
            res =  (1./tau + V_GetCmp(x_v2, i)/(2*h2)); //
            Q_SetEntry(A_g, i, 0, i, res);
            
            res =  (-V_GetCmp(x_v2, down[i])/(2*h2));
            Q_SetEntry(A_g, i, 1, down[i], res);
        break;
    }
}

//b
void b_v1_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    if(status[i] == 0)
        res = exp(V_GetCmp(x_g, i))*V_GetCmp(x_v1, i)/tau + 
            V_GetCmp(x_v1, i)*V_GetCmp(x_v1, i)*(exp(V_GetCmp(x_g, i+1)) - exp(V_GetCmp(x_g, i-1)))/(6*h1) + 
            V_GetCmp(x_v1, i)*( exp(V_GetCmp(x_g, up[i]))*V_GetCmp(x_v2, up[i]) - exp(V_GetCmp(x_g, down[i]))*V_GetCmp(x_v2, down[i]))/(4*h2) - 
            arg.C*(exp(V_GetCmp(x_g, i+1)) - exp(V_GetCmp(x_g, i-1)))/(2*h1) +
            arg.Mu/3.*( V_GetCmp(x_v2, up[i] + 1) - V_GetCmp(x_v2, up[i] - 1) - V_GetCmp(x_v2, down[i] + 1) + V_GetCmp(x_v2, down[i] - 1) )/(4*h1*h2);
    else
        if(status[i] == 13)
            res = arg.omega;
        else
            res = 0.;
    
    V_SetCmp(b_v1, i, res);
}

void b_v2_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{       
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    if(status[i] == 0)
        res = exp(V_GetCmp(x_g, i))*V_GetCmp(x_v2, i)/tau + 
            V_GetCmp(x_v2, i)*V_GetCmp(x_v2, i)*(exp(V_GetCmp(x_g, up[i])) - exp(V_GetCmp(x_g, down[i])))/(6*h2) + 
            V_GetCmp(x_v2, i)*( exp(V_GetCmp(x_g, i+1))*V_GetCmp(x_v1, i+1) - exp(V_GetCmp(x_g, i-1))*V_GetCmp(x_v1, i-1))/(4*h1) - 
            arg.C*(exp(V_GetCmp(x_g, up[i])) - exp(V_GetCmp(x_g, down[i])))/(2*h2) +
            arg.Mu/3.*( V_GetCmp(x_v1, up[i] + 1) - V_GetCmp(x_v1, up[i] - 1) - V_GetCmp(x_v1, down[i] + 1) + V_GetCmp(x_v1, down[i] - 1) )/(4*h1*h2);
    else
        res = 0.;
    
    V_SetCmp(b_v2, i, res);
}

void b_g_las_leak_next(int i, QMatrix_L * A_g, QMatrix_L * A_v1, QMatrix_L * A_v2, Vector * x_g, Vector * x_v1, Vector * x_v2, Vector * b_g, Vector * b_v1, Vector * b_v2, Args arg, int *status, int * down, int * up)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;

    switch ( status[i] ) 
    {
        case 0:
            res = 
                V_GetCmp(x_g, i)/tau + (V_GetCmp(x_g, i)-2)*( (V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i-1))/(4*h1) + (V_GetCmp(x_v2, up[i]) - V_GetCmp(x_v2, down[i]))/(4*h2) );
        break;

        case 1: //x-
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i))/h1) + 
            0.5*h1*( (V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1) + V_GetCmp(x_g, i)*V_GetCmp(x_v1, i))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i+3)*V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) + V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_v1, i+1) + V_GetCmp(x_v1, i))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_v1, i+2) + V_GetCmp(x_v1, i+1))/(h1*h1)) );
        break;

        case 3: //y-
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, up[i]) - V_GetCmp(x_v2, i))/h2) + 
            0.5*h2*( (V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_g, i)*V_GetCmp(x_v2, i))/(h2*h2) -
                   0.5*(V_GetCmp(x_g, up[up[up[i]]])*V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_v2, i))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_v2, up[i]))/(h2*h2)) );
        break;

        case 2: //x+
            res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i) - V_GetCmp(x_v1, i-1))/h1) - 
            0.5*h1*( (V_GetCmp(x_g, i)*V_GetCmp(x_v1, i) - 2*V_GetCmp(x_g, i-1)*V_GetCmp(x_v1, i-1) + V_GetCmp(x_g, i-2)*V_GetCmp(x_v1, i-2))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i-1)*V_GetCmp(x_v1, i-1) - 2*V_GetCmp(x_g, i-2)*V_GetCmp(x_v1, i-2) + V_GetCmp(x_g, i-3)*V_GetCmp(x_v1, i-3))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i) - 2*V_GetCmp(x_v1, i-1) + V_GetCmp(x_v1, i-2))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i-1) - 2*V_GetCmp(x_v1, i-2) + V_GetCmp(x_v1, i-3))/(h1*h1)) );
        break;

        case 4: //y+
            res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, i) - V_GetCmp(x_v2, down[i]))/h2) - 
            0.5*h2*( (V_GetCmp(x_g, i)*V_GetCmp(x_v2, i) - 2*V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_g, down[down[down[i]]])*V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, i) - 2*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_v2, down[down[i]]))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2)) );
        break;


        case 5: //x- y-
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i))/h1) + 
            0.5*h1*( (V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1) + V_GetCmp(x_g, i)*V_GetCmp(x_v1, i))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i+3)*V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) + V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_v1, i+1) + V_GetCmp(x_v1, i))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_v1, i+2) + V_GetCmp(x_v1, i+1))/(h1*h1)) );
        break;

        case 7: //x- y+
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i))/h1) + 
            0.5*h1*( (V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1) + V_GetCmp(x_g, i)*V_GetCmp(x_v1, i))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i+3)*V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) + V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_v1, i+1) + V_GetCmp(x_v1, i))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_v1, i+2) + V_GetCmp(x_v1, i+1))/(h1*h1)) );
        break;


        case 6: //x+ y-
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, up[i]) - V_GetCmp(x_v2, i))/h2) + 
            0.5*h2*( (V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_g, i)*V_GetCmp(x_v2, i))/(h2*h2) -
                   0.5*(V_GetCmp(x_g, up[up[up[i]]])*V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_v2, i))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_v2, up[i]))/(h2*h2)) );
        break;

        case 8: //x+ y+
            res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, i) - V_GetCmp(x_v2, down[i]))/h2) - 
            0.5*h2*( (V_GetCmp(x_g, i)*V_GetCmp(x_v2, i) - 2*V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_g, down[down[down[i]]])*V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, i) - 2*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_v2, down[down[i]]))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2)) );
        break;


        ///////////
        case 10:
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i))/h1) + 
            0.5*h1*( (V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1) + V_GetCmp(x_g, i)*V_GetCmp(x_v1, i))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i+3)*V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) + V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_v1, i+1) + V_GetCmp(x_v1, i))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_v1, i+2) + V_GetCmp(x_v1, i+1))/(h1*h1)) );
        break;

        case 11:
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v1, i+1) - V_GetCmp(x_v1, i))/h1) + 
            0.5*h1*( (V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1) + V_GetCmp(x_g, i)*V_GetCmp(x_v1, i))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, i+3)*V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_g, i+2)*V_GetCmp(x_v1, i+2) + V_GetCmp(x_g, i+1)*V_GetCmp(x_v1, i+1))/(h1*h1) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v1, i+2) - 2*V_GetCmp(x_v1, i+1) + V_GetCmp(x_v1, i))/(h1*h1) -
                                  0.5*(V_GetCmp(x_v1, i+3) - 2*V_GetCmp(x_v1, i+2) + V_GetCmp(x_v1, i+1))/(h1*h1)) );
        break;

        ///////////
        case 12: //y-
             res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, up[i]) - V_GetCmp(x_v2, i))/h2) + 
            0.5*h2*( (V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_g, i)*V_GetCmp(x_v2, i))/(h2*h2) -
                   0.5*(V_GetCmp(x_g, up[up[up[i]]])*V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_g, up[up[i]])*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_g, up[i])*V_GetCmp(x_v2, up[i]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, up[up[i]]) - 2*V_GetCmp(x_v2, up[i]) + V_GetCmp(x_v2, i))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, up[up[up[i]]]) - 2*V_GetCmp(x_v2, up[up[i]]) + V_GetCmp(x_v2, up[i]))/(h2*h2)) );
        break;

        case 13: //x-
             res =
                 log(RHO_0);
        break;


        case 14: //y+
            res =
                 V_GetCmp(x_g, i)/tau + 0.5*((V_GetCmp(x_g, i) - 2)*(V_GetCmp(x_v2, i) - V_GetCmp(x_v2, down[i]))/h2) - 
            0.5*h2*( (V_GetCmp(x_g, i)*V_GetCmp(x_v2, i) - 2*V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]))/(h1*h1) -
                   0.5*(V_GetCmp(x_g, down[i])*V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_g, down[down[i]])*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_g, down[down[down[i]]])*V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2) +
                   (2-V_GetCmp(x_g, i))*( (V_GetCmp(x_v2, i) - 2*V_GetCmp(x_v2, down[i]) + V_GetCmp(x_v2, down[down[i]]))/(h2*h2) -
                                  0.5*(V_GetCmp(x_v2, down[i]) - 2*V_GetCmp(x_v2, down[down[i]]) + V_GetCmp(x_v2, down[down[down[i]]]))/(h2*h2)) );
        break;
    }
    V_SetCmp(b_g, i, res);
}