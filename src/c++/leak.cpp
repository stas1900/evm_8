#include"solve.hpp"
#include"test.hpp"



int main(int argc,char *argv[])
{
    int M1, M2, iter_max, verbose;
    double Mu, C, tau, eps, omega, eps_end, max_t, t_end, m;

    if(argc!=12){
        printf("USAGE: %s <M1> <M2> <tau> <Mu> <C> <eps> <max> <verbose> <omega> <eps_end> <max_t> \n",argv[0]);
    return -1;}

    
    // Моя область:
    // - - +
    // + + +
    // - + +
    
    if(argc == 12)
    {
        if(toInt(argv[1], &M1)!=0       || 
           toInt(argv[2], &M2)!=0       || 
           toDouble(argv[3], &tau)!=0     ||
           toDouble(argv[4], &Mu)!=0    ||
           toDouble(argv[5], &C)!=0     || 
           toDouble(argv[6], &eps)!=0   ||
           toInt(argv[7], &iter_max)!=0 ||
           toInt(argv[8], &verbose)!=0  ||
           toDouble(argv[9], &omega)!=0 ||
           toDouble(argv[10], &eps_end)!=0 || 
           toDouble(argv[11], &max_t)!=0
          )
            {
                printf("Incorrect input data\n");
                return -1;
            }

        if(M1%3!=1 || M2%3!=1 || M1==1 || M2==1)
        {
            printf("M1%3!=1 or M2!%3!=1\n");
            return -2; 
        }
        
        double X1 = 3;
        double X2 = 3;
        double t_end;
        Args arg = Args(M1, M2, tau, X1, X2, Mu, C, verbose, omega, eps_end, max_t, eps, iter_max);
        Funcs func;
        func.f0 = f0;
        func.f1 = f1;
        func.f2 = f2;
        
        Initial initial;
        initial.p = p_initial_leak;
        initial.g = g_initial_leak;
        initial.v1 = v1_initial_leak;
        initial.v2 = v2_initial_leak;
        
        Node * Nodes = new Node[arg.size]; 
        double * buf = new double[11*arg.size];
        memset(buf, 0, 11*arg.size*sizeof(double));
        
        
        init(Nodes, arg, initial);
        ext_status(Nodes, arg);
        //print(Nodes, arg);
        
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(Nodes[i].g);
        if(!save_gvv_txt("data/initial.txt", Nodes, arg, 0, m))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        if(arg.verbose == 2)
        {
            fopen("data/query.txt", "w+");
        }
        fopen("data/m.txt", "w+");
        
        calculate_leak(Nodes, buf, arg, func, t_end);
        
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(Nodes[i].g);
        if(!save_gvv_txt("data/last.txt", Nodes, arg, t_end, m))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        delete[] buf;   
        delete[] Nodes;

    }
    return 0;
}
