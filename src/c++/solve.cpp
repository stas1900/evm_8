#include "solve.hpp"
#include "test.hpp"


CGS_res CGS(Node *Nodes, double *buf, Args arg, void (*mult)(Node *, double *, double *,  Args), double (*b_)(Node *, int, Args, double (*f)(double, double, double, double, double), double t), double (*f)(double, double, double, double, double), double t)
{
    double *d = buf;
    double *r = d + arg.size;
    double *z = r + arg.size;
    double *u = z + arg.size;
    double *p = u + arg.size;
    double *q = p + arg.size;
    double *temp1 = q + arg.size;
    double *temp2 = temp1 + arg.size;
    
    //memset(buf, 0, 11*arg.size*sizeof(double));
    
    double S1, S2, a, b, res, b_norm, tmp;
    
    mult(Nodes, d, r, arg); //r = Ad
    
    
    for(int i = 0; i < arg.size; i++)
        r[i] = b_(Nodes, i, arg, f, t) - r[i]; // r = b - r
    

    memcpy(p, r, arg.size*sizeof(double));
    memcpy(u, r, arg.size*sizeof(double));
    
    b_norm = 0.;
    for(int i = 0; i < arg.size; i++)
    {
        tmp = b_(Nodes, i, arg, f, t);
        b_norm += tmp*tmp;
    }
    b_norm = sqrt(b_norm);
    if (b_norm < 1e-300)
    {
        //printf("ERROR!");
        for(int i = 0; i < arg.size; i++)
            d[i] = 0.;
        return CGS_res(0., 0);
    }
    //printf("\nnorm=%e\n", b_norm);
    
    for(int j = 0; j < arg.itermax; j++)
    {

        S1 = S2 = 0.;

        for(int i = 0; i < arg.size; i++) //(r,z)
           S1 += r[i]*z[i]; 

        mult(Nodes, p, q, arg); //q = Ap


        
        for(int i = 0; i < arg.size; i++) //(Ap,z)
           S2 += q[i]*z[i];

        //printf("\n%g \n", S2);
        
        a = S1/S2; // (r,z)/(Ap,z)

        for(int i = 0; i < arg.size; i++) //q = u - aAp
            q[i] = u[i] - a*q[i];

        for(int i = 0; i < arg.size; i++) //t1 = u + q
            temp1[i] = u[i] + q[i];

        for(int i = 0; i < arg.size; i++) //d = d + a(t1)
            d[i] = d[i] + a*(temp1[i]);

        mult(Nodes, temp1, temp2, arg); //t2 = At1

        S1 = S2 = 0.;

        for(int i = 0; i < arg.size; i++) //(r,z)
            S2 += r[i]*z[i];

        for(int i = 0; i < arg.size; i++) //r = r - at2
            r[i] = r[i] - a*temp2[i];

        for(int i = 0; i < arg.size; i++) //(r,z)
            S1 += r[i]*z[i];

        b = S1/S2; // (r_{+1},z)/(r,z)

        for(int i = 0; i < arg.size; i++) //u = r + bq
            u[i] = r[i] + b*q[i];

        for(int i = 0; i < arg.size; i++) //p = u + b(q + bp)
            p[i] = u[i] + b*(q[i] + b*p[i]);

        res = 0.;

        for(int i = 0; i < arg.size; i++)
        {
            if(fabs(r[i]) > res)
                res = fabs(r[i]);
            //printf("%lf\n", r[i]);
        }

        //printf("\033[0G    iter: %d  res: %e ",j, res);
        //printf("\033[0G                                                                        ");
        if(res < arg.eps*b_norm)
            return CGS_res(res, j+1);
    }
    return CGS_res(res, arg.itermax);
}

void calculate_test(Node *Nodes, double *buf, Args arg, Funcs func)
{
    double *d = buf;
    double *r = d + arg.size;
    double *z = r + arg.size;
    double *u = z + arg.size;
    double *p = u + arg.size;
    double *q = p + arg.size;
    double *temp1 = q + arg.size;
    double *temp2 = temp1 + arg.size;
    
    double *g = temp2 + arg.size;
    double *v1 = g + arg.size;
    double *v2 = v1 + arg.size;
    
    for(int i = 0; i < arg.size; i++)
        z[i] = (i%2? 0. : 1.);
    
    double S1, S2, a, b, res;
    CGS_res warn_g, warn_v1, warn_v2;
    double tau = arg.tau;
    
    double m = 0;
    for(int i = 0; i < arg.size; i++)
        m += arg.h1*arg.h2*exp(Nodes[i].g);
    
    if(arg.verbose == 2)
        save_gvv_txt_plus("data/query.txt", Nodes, arg, 0., m);
    save_m_txt_plus("data/m.txt",  0., m);
    
    clock_t timer = clock();
    int total_iter_g = 0, total_iter_v1 = 0, total_iter_v2 = 0;
    for(int s=1; s <= arg.N; s++)
    {
        if(arg.verbose != 0)
            printf("t = %-10g  res ::::", s*arg.tau);
        
        //G
        
        warn_g = CGS(Nodes, buf, arg, mult_matrix_g_0,  b_g_0,  func.f0, (s-1)*arg.tau);
        total_iter_g += warn_g.it;
        res = 0.;
        for(int i = 0; i < arg.size; i++)
            if(fabs(g_true(Nodes[i].x, Nodes[i].y, s*tau)  - d[i]) > res)
                res = fabs(g_true(Nodes[i].x, Nodes[i].y, s*tau) - d[i]);
        if(arg.verbose != 0)
            printf("    g:%15e   ",res);
        memcpy(g, d, arg.size*sizeof(double));
        
        //V1
        warn_v1 = CGS(Nodes, buf, arg, mult_matrix_v1_0, b_v1_0, func.f1, (s-1)*arg.tau);
        total_iter_v1 += warn_v1.it;
        res = 0.;
        for(int i = 0; i < arg.size; i++)
            if(fabs(v1_true(Nodes[i].x, Nodes[i].y, s*tau) - d[i]) > res)
                res = fabs(v1_true(Nodes[i].x, Nodes[i].y, s*tau) - d[i]);
        if(arg.verbose != 0)
            printf("   v1:%15e   ",res);
        memcpy(v1, d, arg.size*sizeof(double));
        
        //V2
        warn_v2 = CGS(Nodes, buf, arg, mult_matrix_v2_0, b_v2_0, func.f2, (s-1)*arg.tau);
        total_iter_v2 += warn_v2.it;
        res = 0.;
        for(int i = 0; i < arg.size; i++)
            if(fabs(v2_true(Nodes[i].x, Nodes[i].y, s*tau) - d[i]) > res)
                res = fabs(v2_true(Nodes[i].x, Nodes[i].y, s*tau) - d[i]);
        if(arg.verbose != 0)
            printf("   v2:%15e   ",res);
        memcpy(v2, d, arg.size*sizeof(double));
        
        if(arg.verbose != 0)
            printf("\n            CGS_res ::::");
        
        if(arg.verbose != 0)
        {
            printf("     %e it=%d" , warn_g.res, warn_g.it);
            printf("     %e it=%d", warn_v1.res, warn_v1.it);
            printf("     %e it=%d", warn_v2.res, warn_v2.it);
            if(warn_g.it == arg.itermax || warn_v1.it == arg.itermax || warn_v2.it == arg.itermax)
                printf(" Warning!!");
            printf("\n=================================================================================================\n");
        }

        //Omega
        for(int i = 0; i < arg.size; i++)
        {
            
            Nodes[i].g  = g[i];
            //Nodes[i].g  = g_true(Nodes[i].x, Nodes[i].y, s*tau);
            Nodes[i].v1 = v1[i];
            //Nodes[i].v1 = v1_true(Nodes[i].x, Nodes[i].y, s*tau);
            Nodes[i].v2 = v2[i];
            //Nodes[i].v2 = v2_true(Nodes[i].x, Nodes[i].y, s*tau);
        }
        
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(g[i]);
                
        if(arg.verbose == 2)
            save_gvv_txt_plus("data/query.txt", Nodes, arg, s*arg.tau, m);
        save_m_txt_plus("data/m.txt",  s*arg.tau, m);
    }
    timer = clock() - timer;
    if(arg.verbose != 0)
        printf("elapsed work CGS: %f with %d + %d + %d = %d iter. So %e sec per iter\n", ((float)timer)/CLOCKS_PER_SEC, total_iter_g, total_iter_v1, total_iter_v2, total_iter_g + total_iter_v1 + total_iter_v2,  (total_iter_g + total_iter_v1 + total_iter_v2)/(((float)timer)/CLOCKS_PER_SEC));
}

//y = Ax
void mult_matrix_v1_0(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    
    for(int i = 0; i < arg.size; i++)
    {
        if(Nodes[i].status == 0)
            y[i] =
                    x[i-1]*( (-exp(Nodes[i].g)*Nodes[i].v1 - exp(Nodes[i-1].g)*Nodes[i-1].v1 )/(6*h1) - 4./3.*arg.Mu/(h1*h1)) +
                    x[i]*(exp(Nodes[i].g)/tau + 8./3.*arg.Mu/(h1*h1) + 2*arg.Mu/(h2*h2)) + 
                    x[i+1]*( ( exp(Nodes[i].g)*Nodes[i].v1 + exp(Nodes[i+1].g)*Nodes[i+1].v1 )/(6*h1) - 4./3.*arg.Mu/(h1*h1)) +
                    x[Nodes[i].down]*( (-exp(Nodes[i].g)*Nodes[i].v2 -exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2)/(4*h2) - arg.Mu/(h2*h2) ) +
                    x[Nodes[i].up] * ( ( exp(Nodes[i].g)*Nodes[i].v2 + 
exp(Nodes[Nodes[i].up].g  )*Nodes[Nodes[i].up].v2  )/(4*h2) - arg.Mu/(h2*h2) );
        else
            y[i] = x[i];
    }
}

void mult_matrix_v2_0(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    
    for(int i = 0; i < arg.size; i++)
    {
        if(Nodes[i].status == 0)
            y[i] =
                    x[Nodes[i].down]*( (-exp(Nodes[i].g)*Nodes[i].v2 - exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2 )/(6*h2) - 4./3.*arg.Mu/(h2*h2)) +
                    x[i]*(exp(Nodes[i].g)/tau + 8./3.*arg.Mu/(h2*h2) + 2*arg.Mu/(h1*h1)) + 
                    x[Nodes[i].up] * ( ( exp(Nodes[i].g)*Nodes[i].v2 + exp(Nodes[Nodes[i].up].g)*Nodes[Nodes[i].up].v2 )/(6*h2) - 4./3.*arg.Mu/(h2*h2)) +
                    x[i-1]*( (-exp(Nodes[i].g)*Nodes[i].v1 -exp(Nodes[i-1].g)*Nodes[i-1].v1)/(4*h1) - arg.Mu/(h1*h1) ) +
                    x[i+1] * ( ( exp(Nodes[i].g)*Nodes[i].v1 + 
exp(Nodes[i+1].g  )*Nodes[i+1].v1  )/(4*h1) - arg.Mu/(h1*h1) );
        else
            y[i] = x[i];
    }
}

void mult_matrix_g_0(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    
    for(int i = 0; i < arg.size; i++)
    {
        switch ( Nodes[i].status ) 
        {
            case 0:
                y[i] =
                x[Nodes[i].up] * ( Nodes[i].v2 + Nodes[Nodes[i].up].v2   )/(4*h2) +
                x[Nodes[i].down]*(-Nodes[i].v2 - Nodes[Nodes[i].down].v2 )/(4*h2) +
                x[i]*( 1./tau ) +
                x[i - 1] * ( -Nodes[i].v1 - Nodes[i - 1].v1 ) / (4*h1) + 
                x[i + 1] * (  Nodes[i].v1 + Nodes[i + 1].v1 ) / (4*h1);
            break;
                
            case 1: //x-
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 3: //y-
                y[i] =
                x[i]*(1./tau - Nodes[i].v2/(2*h2)) +
                x[Nodes[i].up] * ( Nodes[Nodes[i].up].v2/(2*h2));
            break;
                
            case 2: //x+
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
            case 4: //y+
                y[i] =
                x[i]*(1./tau + Nodes[i].v2/(2*h2)) + //
                    x[Nodes[i].down] * (-Nodes[Nodes[i].down].v2/(2*h2));
            break;
                
                
            case 5: //x- y-
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 7: //x- y+
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
               
            case 6: //x+ y-
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
            case 8: //x+ y+
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
                
            ///////////
            case 10:
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 11:
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
        }
    }
}

//b[i]

double b_v1_0(Node *Nodes, int i, Args arg, double (*f1)(double, double, double, double, double), double t)
{
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    
    if(Nodes[i].status == 0)
        res = exp(Nodes[i].g)*Nodes[i].v1/tau + 
            Nodes[i].v1*Nodes[i].v1*(exp(Nodes[i+1].g) - exp(Nodes[i-1].g))/(6*h1) + 
            Nodes[i].v1*( exp(Nodes[Nodes[i].up].g)*Nodes[Nodes[i].up].v2 - exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2 )/(4*h2) - 
            arg.C*(exp(Nodes[i+1].g) - exp(Nodes[i-1].g))/(2*h1) +
            arg.Mu/3.*( Nodes[Nodes[i].up + 1].v2 - Nodes[Nodes[i].up - 1].v2 - Nodes[Nodes[i].down + 1].v2 + Nodes[Nodes[i].down - 1].v2 )/(4*h1*h2)+
            exp(Nodes[i].g)*f1(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
    else
        res = 0.;
    
    return res;
}

double b_v2_0(Node *Nodes, int i, Args arg, double (*f2)(double, double, double, double, double), double t)
{
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    
    if(Nodes[i].status == 0)
        res = exp(Nodes[i].g)*Nodes[i].v2/tau + 
                Nodes[i].v2*Nodes[i].v2*(exp(Nodes[Nodes[i].up].g) - exp(Nodes[Nodes[i].down].g))/(6.*h2) + 
                Nodes[i].v2*(exp(Nodes[i+1].g)*Nodes[i+1].v1 - exp(Nodes[i-1].g)*Nodes[i-1].v1)/(4.*h1) - 
                arg.C*(exp(Nodes[Nodes[i].up].g) - exp(Nodes[Nodes[i].down].g))/(2.*h2) +
                arg.Mu/3.*(Nodes[Nodes[i].up + 1].v1 - Nodes[Nodes[i].up - 1].v1 - Nodes[Nodes[i].down + 1].v1 + Nodes[Nodes[i].down - 1].v1)/(4.*h1*h2)+
                exp(Nodes[i].g)*f2(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
    else
        res = 0.;
    
    return res;
}

double b_g_0(Node *Nodes, int i, Args arg, double (*f0)(double, double, double, double, double), double t)
{
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    
    switch ( Nodes[i].status ) 
    {
        case 0:
            res = 
                Nodes[i].g/tau + (Nodes[i].g-2)*( (Nodes[i+1].v1 - Nodes[i-1].v1)/(4*h1) + (Nodes[Nodes[i].up].v2 - Nodes[Nodes[i].down].v2)/(4*h2) ) + f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 1: //x-
             res = 
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 3: //y-
             res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[Nodes[i].up].v2 - Nodes[i].v2)/h2) + 
            0.5*h2*( (Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2 + Nodes[i].g*Nodes[i].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].g*Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].v2 + Nodes[i].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].v2)/(h2*h2)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 2: //x+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v1 - Nodes[i-1].v1)/h1) - 
            0.5*h1*( (Nodes[i].g*Nodes[i].v1 - 2*Nodes[i-1].g*Nodes[i-1].v1 + Nodes[i-2].g*Nodes[i-2].v1)/(h1*h1) -
                   0.5*(Nodes[i-1].g*Nodes[i-1].v1 - 2*Nodes[i-2].g*Nodes[i-2].v1 + Nodes[i-3].g*Nodes[i-3].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i].v1 - 2*Nodes[i-1].v1 + Nodes[i-2].v1)/(h1*h1) -
                                  0.5*(Nodes[i-1].v1 - 2*Nodes[i-2].v1 + Nodes[i-3].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 4: //y+
            res = 
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v2 - Nodes[Nodes[i].down].v2)/h2) -
            0.5*h2*( (Nodes[i].g*Nodes[i].v2 - 2*Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                   0.5*( Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].g*Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[i].v2 - 2*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2)  -                     0.5*(Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;


        case 5: //x- y-
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 7: //x- y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) +
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;


        case 6: //x+ y-
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[Nodes[i].up].v2 - Nodes[i].v2)/h2) + 
            0.5*h2*( (Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2 + Nodes[i].g*Nodes[i].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].g*Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].v2 + Nodes[i].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].v2)/(h2*h2)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 8: //x+ y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v2 - Nodes[Nodes[i].down].v2)/h2) -
            0.5*h2*( (Nodes[i].g*Nodes[i].v2 - 2*Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].g*Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[i].v2 - 2*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;


        ///////////
        case 10:
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

        case 11:
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) + 
            f0(Nodes[i].x, Nodes[i].y, t, arg.C, arg.Mu);
        break;

    }
    return res;
}


///////////////////////////////////////////////////////////////////LEAK///////////////////////////////////////////////////



void calculate_leak(Node *Nodes, double *buf, Args arg, Funcs func, double & t_end)
{
    double *d = buf;
    double *r = d + arg.size;
    double *z = r + arg.size;
    double *u = z + arg.size;
    double *p = u + arg.size;
    double *q = p + arg.size;
    double *temp1 = q + arg.size;
    double *temp2 = temp1 + arg.size;
    
    double *g = temp2 + arg.size;
    double *v1 = g + arg.size;
    double *v2 = v1 + arg.size;
    
    for(int i = 0; i < arg.size; i++)
        z[i] = d[i] = (i%2? 0. : 1.);
    
    double S1, S2, a, b, res, m_prev, m_cur, res_g, res_v1, res_v2;
    CGS_res warn_g, warn_v1, warn_v2;
    
    double tau = arg.tau;
    
    
    m_prev = 0;
    for(int i = 0; i < arg.size; i++)
        m_prev += arg.h1*arg.h2*exp(Nodes[i].g);
    
    if(arg.verbose == 2)
        save_gvv_txt_plus("data/query.txt", Nodes, arg, 0., m_prev);
    
    save_m_txt_plus("data/m.txt", 0., m_prev);
    
    clock_t timer = clock();
    int total_iter_g = 0, total_iter_v1 = 0, total_iter_v2 = 0;
    
    for(int s=1; !(s*arg.tau > arg.max_t); s++)
    {   
        double t = s*arg.tau;
        
        //G
        /*for(int i = 0; i < arg.size; i++)
            d[i] = Nodes[i].g + (i%2?1e-10:0.);*/
        /*printf("\n");
        for(int i = 0; i < arg.size; i++)
            printf("%d:%f ", i, b_v1_leak(Nodes, i, arg, func.f0, 0.));;
        printf("\n");*/
        warn_g = CGS(Nodes, buf, arg, mult_matrix_g_leak,  b_g_leak,  func.f0, (s-1)*arg.tau);
        total_iter_g += warn_g.it;
        memcpy(g, d, arg.size*sizeof(double));
        
        //V1
        /*for(int i = 0; i < arg.size; i++)
            d[i] = Nodes[i].v1 + (i%2?1e-10:0.);*/
        warn_v1 = CGS(Nodes, buf, arg, mult_matrix_v1_leak, b_v1_leak, func.f1, (s-1)*arg.tau);
        total_iter_v1 += warn_v1.it;
        memcpy(v1, d, arg.size*sizeof(double));
        
        //V2
        /*for(int i = 0; i < arg.size; i++)
            d[i] = Nodes[i].v2 + (i%2?1e-10:0.);*/
        warn_v2 = CGS(Nodes, buf, arg, mult_matrix_v2_leak, b_v2_leak, func.f2, (s-1)*arg.tau);
        total_iter_v2 += warn_v2.it;
        memcpy(v2, d, arg.size*sizeof(double));
        
        
        if(arg.verbose != 0)
            printf("\n t = %-10g   prev_res ::::", t);
        res_g = 0.;
        for(int i = 0; i < arg.size; i++)
        {
            if(res_g < fabs(Nodes[i].g - g[i]))
                res_g = fabs(Nodes[i].g - g[i]);
        }
        
        if(arg.verbose != 0)
            printf("     %e     " , res_g);
        
        res_v1 = 0.;
        for(int i = 0; i < arg.size; i++)
        {
            if(res_v1 < fabs(Nodes[i].v1 - v1[i]))
                res_v1 = fabs(Nodes[i].v1 - v1[i]);
        }
        
        if(arg.verbose != 0)
            printf("     %e     " , res_v1);
        
        res_v2 = 0.;
        for(int i = 0; i < arg.size; i++)
        {
            if(res_v2 < fabs(Nodes[i].v2 - v2[i]))
                res_v2 = fabs(Nodes[i].v2 - v2[i]);
        }
               
        if(arg.verbose != 0)
        {
            printf("     %e     " , res_v2);
            printf("\n");
        }
        
        m_cur = 0.;
        for(int i = 0; i < arg.size; i++)
            m_cur += arg.h1*arg.h2*exp(g[i]);
        
        
        if(arg.verbose != 0)
            printf(" dm = %e CGS_res ::::", m_cur - m_prev);
        m_prev = m_cur;
        
        if(arg.verbose != 0)
        {
            printf("     %e it=%d" , warn_g.res, warn_g.it);
            printf("     %e it=%d", warn_v1.res, warn_v1.it);
            printf("     %e it=%d", warn_v2.res, warn_v2.it);
            if(warn_g.it == arg.itermax || warn_v1.it == arg.itermax || warn_v2.it == arg.itermax)
                printf(" Warning!!");
            printf("\n=================================================================================================\n");
        }

        //Omega
                
        for(int i = 0; i < arg.size; i++)
        {
            Nodes[i].g  = g[i];
            Nodes[i].v1 = v1[i];
            Nodes[i].v2 = v2[i];
        }
        
        if(arg.verbose == 2)
            save_gvv_txt_plus("data/query.txt", Nodes, arg, s*arg.tau, m_prev);
        
        save_m_txt_plus("data/m.txt",  s*arg.tau, m_prev);
        
        t_end = s*arg.tau;
        
        if(res_g < arg.eps_end && res_v1 < arg.eps_end && res_v2 < arg.eps_end)
        {
            timer = clock() - timer;
            if(arg.verbose != 0)
                printf("elapsed work CGS: %f with %d + %d + %d = %d iter. So %e sec per iter\n", ((float)timer)/CLOCKS_PER_SEC, total_iter_g, total_iter_v1, total_iter_v2, total_iter_g + total_iter_v1 + total_iter_v2,  (total_iter_g + total_iter_v1 + total_iter_v2)/(((float)timer)/CLOCKS_PER_SEC));
            return;
        }
    }
    timer = clock() - timer;
    if(arg.verbose != 0)
        printf("elapsed work CGS: %f with %d + %d + %d = %d iter. So %e sec per iter\n", ((float)timer)/CLOCKS_PER_SEC, total_iter_g, total_iter_v1, total_iter_v2, total_iter_g + total_iter_v1 + total_iter_v2,  (total_iter_g + total_iter_v1 + total_iter_v2)/(((float)timer)/CLOCKS_PER_SEC));
    return;
}







//y = Ax
void mult_matrix_v1_leak(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    
    for(int i = 0; i < arg.size; i++)
    {
        if(Nodes[i].status == 0)
            y[i] =
                    x[i-1]*( (-exp(Nodes[i].g)*Nodes[i].v1 - exp(Nodes[i-1].g)*Nodes[i-1].v1 )/(6*h1) - 4./3.*arg.Mu/(h1*h1)) +
                    x[i]*(exp(Nodes[i].g)/tau + 8./3.*arg.Mu/(h1*h1) + 2*arg.Mu/(h2*h2)) + 
                    x[i+1]*( ( exp(Nodes[i].g)*Nodes[i].v1 + exp(Nodes[i+1].g)*Nodes[i+1].v1 )/(6*h1) - 4./3.*arg.Mu/(h1*h1)) +
                    x[Nodes[i].down]*( (-exp(Nodes[i].g)*Nodes[i].v2 -exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2)/(4*h2) - arg.Mu/(h2*h2) ) +
                    x[Nodes[i].up] * ( ( exp(Nodes[i].g)*Nodes[i].v2 + 
exp(Nodes[Nodes[i].up].g  )*Nodes[Nodes[i].up].v2  )/(4*h2) - arg.Mu/(h2*h2) );
        else
            y[i] = x[i];
    }
}

void mult_matrix_v2_leak(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    
    for(int i = 0; i < arg.size; i++)
    {
        if(Nodes[i].status == 0)
            y[i] =
                    x[Nodes[i].down]*( (-exp(Nodes[i].g)*Nodes[i].v2 - exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2 )/(6*h2) - 4./3.*arg.Mu/(h2*h2)) +
                    x[i]*(exp(Nodes[i].g)/tau + 8./3.*arg.Mu/(h2*h2) + 2*arg.Mu/(h1*h1)) + 
                    x[Nodes[i].up] * ( ( exp(Nodes[i].g)*Nodes[i].v2 + exp(Nodes[Nodes[i].up].g)*Nodes[Nodes[i].up].v2 )/(6*h2) - 4./3.*arg.Mu/(h2*h2)) +
                    x[i-1]*( (-exp(Nodes[i].g)*Nodes[i].v1 -exp(Nodes[i-1].g)*Nodes[i-1].v1)/(4*h1) - arg.Mu/(h1*h1) ) +
                    x[i+1] * ( ( exp(Nodes[i].g)*Nodes[i].v1 + 
exp(Nodes[i+1].g  )*Nodes[i+1].v1  )/(4*h1) - arg.Mu/(h1*h1) );
        else
        {
            if(Nodes[i].status == 12)
                y[i] = (x[Nodes[i].up] - x[i])/(h2);
            else
                if(Nodes[i].status == 14)
                    y[i] = (x[i] - x[Nodes[i].down])/(h2);
                else
                    y[i] = x[i];
        }
    }
}

void mult_matrix_g_leak(Node * Nodes, double *x, double *y, Args arg)
{ 
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    for(int i = 0; i < arg.size; i++)
    {
        switch ( Nodes[i].status ) 
        {
            case 0:
                y[i] =
                x[Nodes[i].up] * ( Nodes[i].v2 + Nodes[Nodes[i].up].v2   )/(4*h2) +
                x[Nodes[i].down]*(-Nodes[i].v2 - Nodes[Nodes[i].down].v2 )/(4*h2) +
                x[i]*( 1./tau ) +
                x[i - 1] * ( -Nodes[i].v1 - Nodes[i - 1].v1 ) / (4*h1) + 
                x[i + 1] * (  Nodes[i].v1 + Nodes[i + 1].v1 ) / (4*h1);
            break;
                
            case 1: //x-
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 3: //y-
                y[i] =
                x[i]*(1./tau - Nodes[i].v2/(2*h2)) +
                x[Nodes[i].up] * ( Nodes[Nodes[i].up].v2/(2*h2));
            break;
                
            case 2: //x+
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
            case 4: //y+
                y[i] =
                x[i]*(1./tau + Nodes[i].v2/(2*h2)) + //
                    x[Nodes[i].down] * (-Nodes[Nodes[i].down].v2/(2*h2));
            break;
                
                
            case 5: //x- y-
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 7: //x- y+
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
               
            case 6: //x+ y-
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
            case 8: //x+ y+
                y[i] =
                x[i]*(1./tau + Nodes[i].v1/(2*h1)) + //
                x[i - 1] * (-Nodes[i-1].v1/(2*h1));
            break;
                
                
            ///////////
            case 10:
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 11:
                y[i] =
                x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            ///////////
            case 12: //y-
                y[i] =
                x[i]*(1./tau - Nodes[i].v2/(2*h2)) +
                x[Nodes[i].up] * ( Nodes[Nodes[i].up].v2/(2*h2));
            break;
                
            case 13: //x-
                y[i] = x[i]; 
                //x[i]*(1./tau - Nodes[i].v1/(2*h1)) +
                //x[i + 1] * ( Nodes[i+1].v1/(2*h1));
            break;
                
            case 14: //y+
                y[i] =
                x[i]*(1./tau + Nodes[i].v2/(2*h2)) + //
                x[Nodes[i].down] * (-Nodes[Nodes[i].down].v2/(2*h2));
            break;
        }
    }
}

//b[i]
double b_v1_leak(Node * Nodes, int i, Args arg, double (*f1)(double, double, double, double, double), double t)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    
    if(Nodes[i].status == 0)
        res = exp(Nodes[i].g)*Nodes[i].v1/tau + 
            Nodes[i].v1*Nodes[i].v1*(exp(Nodes[i+1].g) - exp(Nodes[i-1].g))/(6*h1) + 
            Nodes[i].v1*( exp(Nodes[Nodes[i].up].g)*Nodes[Nodes[i].up].v2 - exp(Nodes[Nodes[i].down].g)*Nodes[Nodes[i].down].v2 )/(4*h2) - 
            arg.C*(exp(Nodes[i+1].g) - exp(Nodes[i-1].g))/(2*h1) +
            arg.Mu/3.*( Nodes[Nodes[i].up + 1].v2 - Nodes[Nodes[i].up - 1].v2 - Nodes[Nodes[i].down + 1].v2 + Nodes[Nodes[i].down - 1].v2 )/(4*h1*h2);
    else
        if(Nodes[i].status == 13)
            res = arg.omega;
        else
            res = 0.;
    
    return res;
}

double b_v2_leak(Node * Nodes, int i, Args arg, double (*f2)(double, double, double, double, double), double t)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;
    
    if(Nodes[i].status == 0)
        res = exp(Nodes[i].g)*Nodes[i].v2/tau + 
            Nodes[i].v2*Nodes[i].v2*(exp(Nodes[Nodes[i].up].g) - exp(Nodes[Nodes[i].down].g))/(6.*h2) + 
            Nodes[i].v2*(exp(Nodes[i+1].g)*Nodes[i+1].v1 - exp(Nodes[i-1].g)*Nodes[i-1].v1)/(4.*h1) - 
            arg.C*(exp(Nodes[Nodes[i].up].g) - exp(Nodes[Nodes[i].down].g))/(2.*h2) +
            arg.Mu/3.*(Nodes[Nodes[i].up + 1].v1 - Nodes[Nodes[i].up - 1].v1 - Nodes[Nodes[i].down + 1].v1 + Nodes[Nodes[i].down - 1].v1)/(4.*h1*h2);
    else
        res = 0.;
    
    return res;
}

double b_g_leak(Node * Nodes, int i, Args arg, double (*f2)(double, double, double, double, double), double t)
{    
    double h1 = arg.h1;
    double h2 = arg.h2;
    double tau = arg.tau;
    double res;

    switch ( Nodes[i].status ) 
    {
        case 0:
            res = 
                Nodes[i].g/tau + (Nodes[i].g-2)*( (Nodes[i+1].v1 - Nodes[i-1].v1)/(4*h1) + (Nodes[Nodes[i].up].v2 - Nodes[Nodes[i].down].v2)/(4*h2) );
        break;

        case 1: //x-
             res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) );
        break;

        case 3: //y-
             res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[Nodes[i].up].v2 - Nodes[i].v2)/h2) + 
            0.5*h2*( (Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2 + Nodes[i].g*Nodes[i].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].g*Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].v2 + Nodes[i].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].v2)/(h2*h2)) );
        break;

        case 2: //x+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v1 - Nodes[i-1].v1)/h1) - 
            0.5*h1*( (Nodes[i].g*Nodes[i].v1 - 2*Nodes[i-1].g*Nodes[i-1].v1 + Nodes[i-2].g*Nodes[i-2].v1)/(h1*h1) -
                   0.5*(Nodes[i-1].g*Nodes[i-1].v1 - 2*Nodes[i-2].g*Nodes[i-2].v1 + Nodes[i-3].g*Nodes[i-3].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i].v1 - 2*Nodes[i-1].v1 + Nodes[i-2].v1)/(h1*h1) -
                                  0.5*(Nodes[i-1].v1 - 2*Nodes[i-2].v1 + Nodes[i-3].v1)/(h1*h1)) );
        break;

        case 4: //y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v2 - Nodes[Nodes[i].down].v2)/h2) -
            0.5*h2*( (Nodes[i].g*Nodes[i].v2 - 2*Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                   0.5*( Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].g*Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[i].v2 - 2*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2)  -                     0.5*(Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2)) );
        break;


        case 5: //x- y-
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) );
        break;

        case 7: //x- y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) +
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) );
        break;


        case 6: //x+ y-
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[Nodes[i].up].v2 - Nodes[i].v2)/h2) + 
            0.5*h2*( (Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2 + Nodes[i].g*Nodes[i].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].g*Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].v2 + Nodes[i].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].v2)/(h2*h2)) );
        break;

        case 8: //x+ y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v2 - Nodes[Nodes[i].down].v2)/h2) - 
            0.5*h2*( (Nodes[i].g*Nodes[i].v2 - 2*Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].g*Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[i].v2 - 2*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2)) );
        break;


        ///////////
        case 10:
           res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                 0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) );
        break;

        case 11:
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
            0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
                   0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
                   (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
                                  0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) );
        break;

        ///////////
        case 12: //y-
             res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[Nodes[i].up].v2 - Nodes[i].v2)/h2) + 
            0.5*h2*( (Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2 + Nodes[i].g*Nodes[i].v2)/(h2*h2) -
                   0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].g*Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].g*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].g*Nodes[Nodes[i].up].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[Nodes[Nodes[i].up].up].v2 - 2*Nodes[Nodes[i].up].v2 + Nodes[i].v2)/(h2*h2) -
                                  0.5*(Nodes[Nodes[Nodes[Nodes[i].up].up].up].v2 - 2*Nodes[Nodes[Nodes[i].up].up].v2 + Nodes[Nodes[i].up].v2)/(h2*h2)) );
        break;

        case 13: //x-
             res =
                 log(RHO_0);
                 //Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i+1].v1 - Nodes[i].v1)/h1) + 
           // 0.5*h1*( (Nodes[i+2].g*Nodes[i+2].v1 - 2*Nodes[i+1].g*Nodes[i+1].v1 + Nodes[i].g*Nodes[i].v1)/(h1*h1) -
           //        0.5*(Nodes[i+3].g*Nodes[i+3].v1 - 2*Nodes[i+2].g*Nodes[i+2].v1 + Nodes[i+1].g*Nodes[i+1].v1)/(h1*h1) +
           //        (2-Nodes[i].g)*( (Nodes[i+2].v1 - 2*Nodes[i+1].v1 + Nodes[i].v1)/(h1*h1) -
           //                       0.5*(Nodes[i+3].v1 - 2*Nodes[i+2].v1 + Nodes[i+1].v1)/(h1*h1)) ) - x[i];
        break;


        case 14: //y+
            res =
                 Nodes[i].g/tau + 0.5*((Nodes[i].g - 2)*(Nodes[i].v2 - Nodes[Nodes[i].down].v2)/h2) -
            0.5*h2*( (Nodes[i].g*Nodes[i].v2 - 2*Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2) -
                   0.5*( Nodes[Nodes[i].down].g*Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].g*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].g*Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2) +
                   (2-Nodes[i].g)*( (Nodes[i].v2 - 2*Nodes[Nodes[i].down].v2 + Nodes[Nodes[Nodes[i].down].down].v2)/(h2*h2)  -                     0.5*(Nodes[Nodes[i].down].v2 - 2*Nodes[Nodes[Nodes[i].down].down].v2 + Nodes[Nodes[Nodes[Nodes[i].down].down].down].v2)/(h2*h2)) );
        break;
    }
    return res;
}