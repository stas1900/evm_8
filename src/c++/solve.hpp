#ifndef _SOLVE_HPP_
#define _SOLVE_HPP_

#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h> 

#define RHO_0 1

struct Initial;
struct Args;
struct Funcs;
struct Node;
struct CGS_res;
struct Nodes_res;

int toInt(const char *s,int *xp);
int toDouble(const char *s,double *xp);

void init(Node* Nodes, Args arg, Initial initial);
void real(Node* Nodes, Args arg);

void ext_status(Node* Nodes, Args arg);

double p_initial(double x1, double x2);
double g_initial(double x1, double x2);
double v1_initial(double x1, double x2, double omega);
double v2_initial(double x1, double x2);

double p_initial_leak(double x1, double x2);
double g_initial_leak(double x1, double x2);
double v1_initial_leak(double x1, double x2, double omega);
double v2_initial_leak(double x1, double x2);

double p_true(double x1, double x2, double t);
double g_true(double x1, double x2, double t);
double v1_true(double x1, double x2, double t);
double v2_true(double x1, double x2, double t);

double f0(double x1, double x2, double t, double C, double Mu);
double f1(double x1, double x2, double t, double C, double Mu);
double f2(double x1, double x2, double t, double C, double Mu);


int save_gvv_txt(const char* filename, Node* Nodes, Args arg, double t, double m);
int save_gvv_txt_plus(const char* filename, Node* Nodes, Args arg, double t, double m);
int save_m_txt_plus(const char* filename, double t, double m);
void print(Node* Nodes, Args arg);

CGS_res CGS(Node *Nodes, double *buf, Args arg, void (*mult)(Node *, double *, double *,  Args), double (*b_)(Node *, int, Args, double (*f)(double, double, double, double, double), double t), double (*f)(double, double, double, double, double), double t);



void calculate_test(Node *Nodes, double *buf, Args arg, Funcs func);

void mult_matrix_v1_0(Node * GVV, double *x, double *y, Args arg);
void mult_matrix_v2_0(Node * GVV, double *x, double *y, Args arg);
void mult_matrix_g_0 (Node * GVV, double *x, double *y, Args arg);

double b_v1_0(Node *Nodes, int i, Args arg, double (*f1)(double, double, double, double, double), double t);
double b_v2_0(Node *Nodes, int i, Args arg, double (*f2)(double, double, double, double, double), double t);
double b_g_0(Node *Nodes, int i, Args arg, double (*f0)(double, double, double, double, double), double t);



void calculate_leak(Node *Nodes, double *buf, Args arg, Funcs func, double & t_end);

void mult_matrix_v1_leak(Node * GVV, double *x, double *y, Args arg);
void mult_matrix_v2_leak(Node * GVV, double *x, double *y, Args arg);
void mult_matrix_g_leak (Node * GVV, double *x, double *y, Args arg);

double b_v1_leak(Node *Nodes, int i, Args arg, double (*f1)(double, double, double, double, double), double t);
double b_v2_leak(Node *Nodes, int i, Args arg, double (*f2)(double, double, double, double, double), double t);
double b_g_leak(Node *Nodes, int i, Args arg, double (*f0)(double, double, double, double, double), double t);




struct Args
{
    int M1;
    int M2;
    int N;
    
    double X1;
    double X2;
    double T;
    
    double tau;
    double h1;
    double h2;
    
    double Mu;
    double C;
    
    int size;
    int size1;
    int size2;
    int size3;
    
    int n1;
    int m1;
    
    int n2;
    int m2;
    
    int n3;
    int m3;
    
    int verbose;
    
    double omega;
    double eps_end;
    double max_t;
    
    double eps;
    int itermax;
    
    public:
        Args(int M1, int M2, int N, double X1, double X2, double T, double Mu, double C, int verbose, double omega, double eps, int itermax)
        {
            this->M1 = M1;
            this->M2 = M2;
            this->N = N;
            
            this->X1 = X1;
            this->X2 = X2;
            this->T = T;
            
            this->tau = T/N;
            this->h1 = X1/(M1-1);
            this->h2 = X2/(M2-1);
            
            this->Mu = Mu;
            this->C = C;
            
            this->size = ((2*M1+1)*(2*M2+1)-3)/6;
            this->size1 = (M2-1)*(2*M1+1)/9;
            this->size2 = (M2+2)*M1/3;
            this->size3 = (M1+2)*(M2-1)/9;
            
            this->n1 = (M2-1)/3;
            this->m1 = (2*M1+1)/3;
            
            this->n2 = (M2+2)/3;
            this->m2 = M1;
            
            this->n3 = (M2-1)/3;
            this->m3 = (M1+2)/3;
            
            this->verbose = verbose;
            
            this->omega = omega;
            this->max_t = 0;
            this->eps_end = 0;
            
            this->eps = eps;
            this->itermax = itermax;
        }
    
        Args(int M1, int M2, double tau, double X1, double X2, double Mu, double C, int verbose, double omega, double eps_end, double max_t, double eps, int itermax)
        {
            this->M1 = M1;
            this->M2 = M2;
            
            this->X1 = X1;
            this->X2 = X2;
            
            this->tau = tau;
            this->h1 = X1/(M1-1);
            this->h2 = X2/(M2-1);
            
            this->Mu = Mu;
            this->C = C;
            
            this->size = ((2*M1+1)*(2*M2+1)-3)/6;
            this->size1 = (M2-1)*(2*M1+1)/9;
            this->size2 = (M2+2)*M1/3;
            this->size3 = (M1+2)*(M2-1)/9;
            
            this->n1 = (M2-1)/3;
            this->m1 = (2*M1+1)/3;
            
            this->n2 = (M2+2)/3;
            this->m2 = M1;
            
            this->n3 = (M2-1)/3;
            this->m3 = (M1+2)/3;
            
            this->verbose = verbose;
            
            this->omega = omega;
            this->max_t = max_t;
            this->eps_end = eps_end;
            
            this->eps = eps;
            this->itermax = itermax;
        }
};

struct Funcs
{
    double (*f0) (double x1, double x2, double t, double C, double Mu);
    double (*f1) (double x1, double x2, double t, double C, double Mu);
    double (*f2) (double x1, double x2, double t, double C, double Mu);
};

struct Initial
{
    double (*p) (double x1, double x2);
    double (*g) (double x1, double x2);
    double (*v1) (double x1, double x2, double omega);
    double (*v2) (double x1, double x2);
};

struct Node
{
    double g;
    double v1;
    double v2;
    
    int up;
    int down;
    
    int status;
    
    //  status:
    //                     7 4 8             * 10     - *
    //           General:  1 0 2     Angles: -  * and * 11     Red: 12    Blue: 13
    //                     5 3 6 
    
    
    //
    
    double x;
    double y;
};

struct CGS_res
{
    double res;
    int it;
    
    public:
    
        CGS_res()
        {
            this->res = 0;
            this->it = 0;
        }

        CGS_res(double res, int it)
        {
            this->res = res;
            this->it = it;
        }

        CGS_res & operator = (const CGS_res & val) 
        {
            this->res = val.res;
            this->it = val.it;
            return *this;
        }
};

struct Nodes_res
{
    double C_g;
    double C_v1;
    double C_v2;
    
    double L_g;
    double L_v1;
    double L_v2;
    
    double W_g;
    double W_v1;
    double W_v2;
    
    public:
        Nodes_res()
        {
            this->C_g = this->C_v1 = this->C_v2 =  this->L_g = this->L_v1 = this->L_v2 =  this->W_g = this->W_v1 = this->W_v2 = 0.;
        }
    
        Nodes_res(Node * first, Node * second, Args arg)
        {
            this->C_g = this->C_v1 = this->C_v2 =  this->L_g = this->L_v1 = this->L_v2 =  this->W_g = this->W_v1 = this->W_v2 = 0.;
            //C
            for(int i = 0; i < arg.size; i++)
            {
                if(this->C_g < fabs(first[i].g - second[i].g))
                    this->C_g = fabs(first[i].g - second[i].g);
                
                if(this->C_v1 < fabs(first[i].v1 - second[i].v1))
                    this->C_v1 = fabs(first[i].v1 - second[i].v1);
                
                if(this->C_v2 < fabs(first[i].v2 - second[i].v2))
                    this->C_v2 = fabs(first[i].v2 - second[i].v2);
            }
            
            //L
            for(int i = 0; i < arg.size; i++)
            {
                if(first[i].status == 0)
                {
                    L_g += (first[i].g - second[i].g)*(first[i].g - second[i].g);
                    L_v1 += (first[i].v1 - second[i].v1)*(first[i].v1 - second[i].v1);
                    L_v2 += (first[i].v2 - second[i].v2)*(first[i].v2 - second[i].v2);
                }
                else
                {
                    L_g += 0.5*(first[i].g - second[i].g)*(first[i].g - second[i].g);
                    L_v1 += 0.5*(first[i].v1 - second[i].v1)*(first[i].v1 - second[i].v1);
                    L_v2 += 0.5*(first[i].v2 - second[i].v2)*(first[i].v2 - second[i].v2);
                }

            }
                            
            L_g *= arg.h1*arg.h2;
            L_v1 *= arg.h1*arg.h2;
            L_v2 *= arg.h1*arg.h2;

            L_g = sqrt(L_g);
            L_v1 = sqrt(L_v1);
            L_v2 = sqrt(L_v2); 
            
            
            //W
            double W1_g = 0. , W2_g = 0. , W1_v1 = 0. , W2_v1 = 0. , W1_v2 = 0. , W2_v2 = 0. ;
            for(int i = 0; i < arg.size; i++)
            {
                if(first[i].status == 0)// || first[i].status == 1)
                {
                    double temp;
                    
                    temp = (( (first[i+1].g - second[i+1].g) - (first[i].g - second[i].g) )/arg.h1);
                    W1_g += temp*temp;

                    temp = (( (first[i+1].v1 - second[i+1].v1) - (first[i].v1 - second[i].v1) )/arg.h1);
                    W1_v1 += temp*temp;
                    
                    temp = (( (first[i+1].v2 - second[i+1].v2) - (first[i].v2 - second[i].v2) )/arg.h1);
                    W1_v2 += temp*temp;
                    
                }
                
                if(first[i].status == 0) //|| first[i].status == 3)
                {
                    double temp;
                    
                    temp = (( (first[first[i].up].g - second[first[i].up].g) - (first[i].g - second[i].g) )/arg.h2);
                    W2_g += temp*temp;
                    
                    temp = (( (first[first[i].up].v1 - second[first[i].up].v1) - (first[i].v1 - second[i].v1) )/arg.h2);
                    W2_v1 += temp*temp;
                    
                    temp = (( (first[first[i].up].v2 - second[first[i].up].v2) - (first[i].v2 - second[i].v2) )/arg.h2);
                    W2_v2 += temp*temp;
                }
                
                
            }
            
            W_g  += W1_g + W2_g;
            W_v1 += W1_v1 + W2_v1;
            W_v2 += W1_v2 + W2_v2;
                            
            W_g  *= arg.h1*arg.h2;
            W_v1 *= arg.h1*arg.h2;
            W_v2 *= arg.h1*arg.h2;

            W_g = sqrt(W_g + L_g*L_g);
            W_v1 = sqrt(W_v1 + L_v1*L_v1);
            W_v2 = sqrt(W_v2 + L_v2*L_v2); 
        }
};


#endif
