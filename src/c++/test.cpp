#include"solve.hpp"
#include"test.hpp"



int main(int argc,char *argv[])
{
    int M1, M2, iter_max, verbose, N;
    double Mu, C, T, eps, m;

    if(argc!=10){
        printf("USAGE: %s <M1> <M2> <N> <T> <Mu> <C> <eps> <max> <verbose>\n",argv[0]);
    return -1;}

    

    // Считаем по вводу ./a.out M N Mu C
    // Остановимся, когда tau*s => T, tau*(s-1)<T
    // Моя область:
    // - - +
    // + + +
    // - + +
    
    if(argc == 10)
    {
        if(toInt(argv[1], &M1)!=0       || 
           toInt(argv[2], &M2)!=0       || 
           toInt(argv[3], &N)!=0   ||
           toDouble(argv[4], &T)!=0     ||
           toDouble(argv[5], &Mu)!=0    ||
           toDouble(argv[6], &C)!=0     || 
           toDouble(argv[7], &eps)!=0   ||
           toInt(argv[8], &iter_max)!=0 ||
           toInt(argv[9], &verbose)!=0
          )
            {
                printf("Incorrect input data\n");
                return -1;
            }

        if(M1%3!=1 || M2%3!=1 || M1==1 || M2==1)
        {
            printf("M1%3!=1 or M2!%3!=1\n");
            return -2; 
        }
        
        double X1 = 3;
        double X2 = 3;
        
        Args arg = Args(M1, M2, N, X1, X2, T, Mu, C, verbose, 1e-228, eps, iter_max);
        
        Funcs func;
        func.f0 = f0;
        func.f1 = f1;
        func.f2 = f2;
        
        Initial initial;
        initial.p = p_initial;
        initial.g = g_initial;
        initial.v1 = v1_initial;
        initial.v2 = v2_initial;
        
        Node * Nodes = new Node[arg.size]; 
        double * buf = new double[11*arg.size];
        memset(buf, 0, 11*arg.size*sizeof(double));
        
        
        real(Nodes,arg);
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(Nodes[i].g);
        if(!save_gvv_txt("data/real.txt", Nodes, arg, 0, -1))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        init(Nodes, arg, initial);

        //print(Nodes, arg);
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(Nodes[i].g);
        if(!save_gvv_txt("data/initial.txt", Nodes, arg, arg.T, -1))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        if(arg.verbose == 2)
        {
            fopen("data/query.txt", "w+");
        }
        fopen("data/m.txt", "w+");
        calculate_test(Nodes, buf, arg, func);
        
        m = 0.;
        for(int i = 0; i < arg.size; i++)
            m += arg.h1*arg.h2*exp(Nodes[i].g);
        if(!save_gvv_txt("data/last.txt", Nodes, arg, arg.T, m))
        {
            printf("Error! Can't save!\n");
            return -3;
        }
        
        delete[] buf;
        
        Node * Nodes_ = new Node[arg.size]; 
        real(Nodes_, arg);
        
        Nodes_res res = Nodes_res(Nodes, Nodes_, arg);
        
        printf("%g %g %g\n", res.C_g, res.L_g, res.W_g);
        printf("%g %g %g\n", res.C_v1, res.L_v1, res.W_v1);
        printf("%g %g %g\n", res.C_v2, res.L_v2, res.W_v2);
        
        
        delete[] Nodes;
        delete[] Nodes_;

    }
    return 0;
}
