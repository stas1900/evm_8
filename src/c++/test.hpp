#ifndef _TEST_HPP_
#define _TEST_HPP_


//----------------------------------------------------------------------------//


#define P_TRUE ((cos(2*M_PI*x1) + 1.5)*(sin(2*M_PI*x2) + 1.5)*exp(t))
#define G_TRUE (log(P_TRUE))
#define V1_TRUE (sin(2*M_PI*x1)*sin(2*M_PI*x2)*exp(t))
#define V2_TRUE (sin(2*M_PI*x1)*sin(2*M_PI*x2)*exp(-t))

#define P_INITIAL ((cos(2*M_PI*x1) + 1.5)*(sin(2*M_PI*x2) + 1.5))
#define G_INITIAL (log(P_INITIAL))
#define V1_INITIAL (sin(2*M_PI*x1)*sin(2*M_PI*x2))
#define V2_INITIAL (sin(2*M_PI*x1)*sin(2*M_PI*x2))

#define dG_dt (1.)
#define dV1_dt (V1_TRUE)
#define dV2_dt (-V2_TRUE)

#define dG_dx1 (-2*M_PI*(sin(2*M_PI*x1))/(cos(2*M_PI*x1) + 1.5))
#define dV1_dx1 (2*M_PI*cos(2*M_PI*x1)*sin(2*M_PI*x2)*exp(t))
#define dV2_dx1 (2*M_PI*cos(2*M_PI*x1)*sin(2*M_PI*x2)*exp(-t))

#define dG_dx2 ( 2*M_PI*(cos(2*M_PI*x2))/(sin(2*M_PI*x2) + 1.5))
#define dV1_dx2 (2*M_PI*sin(2*M_PI*x1)*cos(2*M_PI*x2)*exp(t))
#define dV2_dx2 (2*M_PI*sin(2*M_PI*x1)*cos(2*M_PI*x2)*exp(-t))

#define d2V1_dx1_dx1 ((-4*M_PI*M_PI)*V1_TRUE)
#define d2V2_dx1_dx1 ((-4*M_PI*M_PI)*V2_TRUE)

#define d2V1_dx2_dx2 ((-4*M_PI*M_PI)*V1_TRUE)
#define d2V2_dx2_dx2 ((-4*M_PI*M_PI)*V2_TRUE)

#define d2V1_dx1_dx2 (4*M_PI*M_PI*cos(2*M_PI*x1)*cos(2*M_PI*x2)*exp(t))
#define d2V2_dx1_dx2 (4*M_PI*M_PI*cos(2*M_PI*x1)*cos(2*M_PI*x2)*exp(-t))


//----------------------------------------------------------------------------//


#define FUNCTION_0 (dG_dt + (V1_TRUE*dG_dx1 + dV1_dx1) + (V2_TRUE*dG_dx2 + dV2_dx2))

#define FUNCTION_1 dV1_dt + (V1_TRUE*dV1_dx1) + (V2_TRUE*dV1_dx2) + C*(dG_dx1) - (Mu/P_TRUE)*((4./3.)*d2V1_dx1_dx1 + d2V1_dx2_dx2 + (1./3.)*(d2V2_dx1_dx2))

#define FUNCTION_2 dV2_dt + (V2_TRUE*dV2_dx2) + (V1_TRUE*dV2_dx1) + C*(dG_dx2) - (Mu/P_TRUE)*((4./3.)*d2V2_dx2_dx2 + d2V2_dx1_dx1 + (1./3.)*(d2V1_dx1_dx2))


//----------------------------------------------------------------------------//


#endif
