from matplotlib import pylab as plt
from matplotlib.ticker import FuncFormatter
from pylab import rcParams
import numpy as np
import math




def print_gvv(filename="data/initial.txt", velocity = False):
    with open(filename, 'r') as file:
        M1, M2 = map(lambda x: int(x), file.readline().split(' ')[:2])
        z = np.array([np.array([float(val) for val in line.split(' ') if val != '\n']) for line in file])
        
        g2p = lambda array:np.array([math.exp(val) for val in array])
        p = np.array(list(map(g2p, z[:M2])))
        v1 = np.array(z[M2:2*M2])
        v2 = np.array(z[2*M2:])
        
        fig, ax = plt.subplots(nrows=1)
        plt.title('Плотность P')
        
        im = ax.pcolormesh(p.transpose())

        fig.colorbar(im, ax=ax)
        
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')
        
        formatter_x = FuncFormatter(lambda x,pos: '%g Pi' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g Pi' % float(3*y/M1))


        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)
        
        if velocity:        
            for i in range(0, M1, int(M1/30 + 1)):
                for j in range(0, M2, int(M2/30 + 1)):
                    ax.arrow(i+0.5, M2 - j - 0.5, v1[i][j], v2[i][j], width = 0.002*M1, head_length = 0.01*M1, fc='r', ec='r')              
        plt.show()