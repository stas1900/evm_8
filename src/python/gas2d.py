from matplotlib import pylab as plt
import matplotlib.pyplot as pltt
from matplotlib.ticker import FuncFormatter
import matplotlib.animation as animation
import numpy as np
import math
from pylab import rcParams


X1 = 3*math.pi
X2 = 3*math.pi

''' В файле такая последовательность:
                      M1 M2 \n
                 {    1 3 2 ... 7 \n
    Это g=log(p) {          ...             (M2 строчек длины М1)
                 {    1 4 2 ... 4 \n
                 [    1 3 2 ... 7 \n
    Это v1       [          ...             (M2 строчек длины М1)
                 [    1 4 2 ... 4 \n
                 {    1 3 2 ... 7 \n
    Это пv2      {          ...             (M2 строчек длины М1)
                 {    1 4 2 ... 4 \n 
                 
    Пример:
                        4 4 
                        1.321756 1.161438 0.662429 -0.089442 
                        1.739085 1.578767 1.079758 0.327888 
                        1.823880 1.663562 1.164553 0.412682 
                        1.586800 1.426482 0.927473 0.175603 
                        0.000000 0.000000 0.000000 0.000000 
                        0.000000 0.603501 0.760027 0.353649 
                        0.000000 0.760027 0.957150 0.445373 
                        0.000000 0.353649 0.445373 0.207237 
                        0.000000 0.000000 0.000000 0.000000 
                        0.000000 0.603501 0.760027 0.353649 
                        0.000000 0.760027 0.957150 0.445373 
                        0.000000 0.353649 0.445373 0.207237 '''


def print_gvv(filename="data/initial.txt", density = 0.98):
    rcParams['figure.figsize'] = 20, 10
    with open(filename, 'r') as file:
        
        string = file.readline().split(' ')
        M1, M2 = map(lambda x: int(x), string[:2])
        M = max(M1,M2)
            
        z = np.array([np.array([float(val) for val in line.split(' ') if val != '\n']) for line in file])
        
        g2p = lambda array:np.array([math.exp(val) for val in array])
        p = np.array(list(map(g2p, z[:M2])))
        v1 = np.array(z[M2:2*M2])
        v2 = np.array(z[2*M2:])
        
        
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j + int((M1-1)/3)] = 0.
        
        
        fig, ax = plt.subplots(nrows=1)
        plt.title('Плотность и скорость\n' + ' '.join(string[2:5]) +'\n' + ' '.join(string[5:]))
    
        
        im = ax.pcolormesh(p)

        fig.colorbar(im, ax=ax)
        
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')
        
        formatter_x = FuncFormatter(lambda x,pos: '%g' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g' % float(3*y/M2))


        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)
        
        for i in range(0, M2, int(M2*(1-density) + 1)):
            for j in range(0, M1, int(M1*(1-density) + 1)):
                if i in range(int((M2-1)/3)) and j in range(int((M1-1)/3)):
                    continue;
                if i in range(int((2*M2+1)/3),M2) and j in range(int((M1-1)/3)):
                    continue;
                if i in range(int((2*M2+1)/3),M2) and j in range(int((M1-1)/3), int((2*M1+1)/3)-1):
                    continue;
                ax.arrow(j+0.5, i+0.5, v1[i][j], v2[i][j], width = 0.002*M, head_length = 0.01*M, fc='r', ec='r')           
        plt.show()
        
        
def print_g(filename="data/initial.txt"):
    rcParams['figure.figsize'] = 20, 10
    with open(filename, 'r') as file:
     
        string = file.readline().split(' ')
        M1, M2 = map(lambda x: int(x), string[:2])
        M = max(M1,M2)
            
        z = np.array([np.array([float(val) for val in line.split(' ') if val != '\n']) for line in file])
        
        g2p = lambda array:np.array([math.exp(val) for val in array])
        p = np.array(list(map(g2p, z[:M2])))
        
        
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j + int((M1-1)/3)] = 0.
        
        
        fig, ax = plt.subplots(nrows=1)
        plt.title('Плотность\n' + ' '.join(string[2:5]) +'\n' + ' '.join(string[5:]))
    
        
        im = ax.pcolormesh(p)

        fig.colorbar(im, ax=ax)
        
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')
        
        formatter_x = FuncFormatter(lambda x,pos: '%g' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g' % float(3*y/M2))


        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)
               
        plt.show()
        
        
def print_v1(filename="data/initial.txt"):
    rcParams['figure.figsize'] = 20, 10
    with open(filename, 'r') as file:
        
        string = file.readline().split(' ')
        M1, M2 = map(lambda x: int(x), string[:2])
        M = max(M1,M2)
            
        z = np.array([np.array([float(val) for val in line.split(' ') if val != '\n']) for line in file])
        
        v1 = np.array(z[M2:2*M2])
        
        
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v1[i][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v1[i+int((2*M2+1)/3)][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v1[i+int((2*M2+1)/3)][j + int((M1-1)/3)] = 0.
        
        
        fig, ax = plt.subplots(nrows=1)
        plt.title('Скорость V1\n' + ' '.join(string[2:5]) +'\n' + ' '.join(string[5:]))
    
        
        im = ax.pcolormesh(v1)

        fig.colorbar(im, ax=ax)
        
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')
        
        formatter_x = FuncFormatter(lambda x,pos: '%g' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g' % float(3*y/M2))


        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)
               
        plt.show()
        
        
def print_v2(filename="data/initial.txt"):
    rcParams['figure.figsize'] = 20, 10
    with open(filename, 'r') as file:
        
        string = file.readline().split(' ')
        M1, M2 = map(lambda x: int(x), string[:2])
        M = max(M1,M2)
            
        z = np.array([np.array([float(val) for val in line.split(' ') if val != '\n']) for line in file])
        
        v2 = np.array(z[2*M2:])
        
        
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v2[i][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v2[i+int((2*M2+1)/3)][j] = 0.
                
        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                v2[i+int((2*M2+1)/3)][j + int((M1-1)/3)] = 0.
        
        
        fig, ax = plt.subplots(nrows=1)
        plt.title('Скорость V2\n' + ' '.join(string[2:5]) +'\n' + ' '.join(string[5:]))
    
        
        im = ax.pcolormesh(v2)

        fig.colorbar(im, ax=ax)
        
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')
        
        formatter_x = FuncFormatter(lambda x,pos: '%g' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g' % float(3*y/M2))


        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)
               
        plt.show()
        
def get_value(any_set):
    any_list = list(any_set)
    if len(any_list) == 0:
        return -1.
    else:
        if any_list[0] == 0.:
            return math.nan
        else:
            return any_list[0]
   
    
    
def res2tex(df, C, Mu, val):
    
    if val not in ['g', 'v1', 'v2']:
        print("Error! Incorrect val. Use: 'g', 'v1', 'v2'");
        return
    
    temp_df = df[df['C'] == C]
    temp_df = temp_df[temp_df['Mu'] == Mu]
    
    M_ = list(set(temp_df['M']));
    M_.sort(reverse = False);
    
    N_ = list(set(temp_df['N']));
    N_.sort(reverse = False);
    
    print("\\begin{table}[!htpb]");
    print("\\centering");
    print("\\caption{Ошибка решения для " + val.upper() + " при $\\mu = %g$ и $C=%g$}" % (Mu, C));
    print("\\begin{center}");
    print("\\begin{tabular}{|c||" + "c|"*len(M_) + "}");
    print("\\hline");
    print("$\\tau\\textbackslash{}h$");
 
    for M in M_:
        #print(temp_df)
        print(" &", 3./(M-1), "\t" , end = '')
    print("\\\\ \\hhline{|=||" + "=|"*(len(M_)) + "}");
    for N in N_: 
        print(1./N, "\t", end='');
        for M in M_:
            cur_df = temp_df[temp_df['M'] == M]
            cur_df = cur_df[cur_df['N'] == N]
            print("& \\begin{tabular}[c]{@{}c@{}}%e\\\\ %e\\\\ %e\\\\ \\end{tabular} \t" % (get_value(set(cur_df['C_'+val])), get_value(set(cur_df['L_'+val])), get_value(set(cur_df['W_'+val]))), end = '');
        print("\\\\ \\hline ");
    print("\\end{tabular}");
    print("\\end{center}");
    print("\\end{table}");
    print("\n");
    
    
def gif(source = "data/query.txt", target = "data/query.gif", fps=60):
    rcParams['figure.figsize'] = 20, 10
    images = []
    label = []

    with open(source, 'r') as file:
        while True:
            firstline = file.readline()
            if firstline == '':
                break
            string = firstline.split(' ')
            M1, M2 = map(lambda x: int(x), string[:2])
            M = max(M1,M2)
            z = np.array([np.array([float(val) for val in next(file).split(' ') if val != '\n']) for N in range(3*M2)])
            images.append(z)
            label.append(string[2:])

    l = len(images)
    fig, ax = plt.subplots(nrows=1)


    def ims(ii):
        rcParams['figure.figsize'] = 20, 10
        ax.clear()
        z = images[ii]
        g2p = lambda array:np.array([math.exp(val) for val in array])
        p = np.array(list(map(g2p, z[:M2])))
        v1 = np.array(z[M2:2*M2])
        v2 = np.array(z[2*M2:])


        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i][j] = 0.

        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j] = 0.

        for i in range(int((M2-1)/3)):
            for j in range(int((M1-1)/3)):
                p[i+int((2*M2+1)/3)][j + int((M1-1)/3)] = 0.


        im = ax.pcolor(p)

        ax.set_title('Плотность и скорость\n' + ' '.join(label[ii][:3]) + '\n' + ' '.join(label[ii][3:]))
        ax.set_xlabel('Абсцисса')
        ax.set_ylabel('Ордината')

        formatter_x = FuncFormatter(lambda x,pos: '%g' % float(3*x/M1))
        formatter_y = FuncFormatter(lambda y,pos: '%g' % float(3*y/M2))

        ax.yaxis.set_major_formatter(formatter_y)
        ax.xaxis.set_major_formatter(formatter_x)

        for i in range(0, M2, int(M2/30 + 1)):
            for j in range(0, M1, int(M1/30 + 1)):
                if i in range(int((M2-1)/3)) and j in range(int((M1-1)/3)):
                    continue;
                if i in range(int((2*M2+1)/3),M2) and j in range(int((M1-1)/3)):
                    continue;
                if i in range(int((2*M2+1)/3),M2) and j in range(int((M1-1)/3), int((2*M1+1)/3)-1):
                    continue;
                ax.arrow(j+0.5, i+0.5, v1[i][j], v2[i][j], width = 0.002*M, head_length = 0.01*M, fc='r', ec='r')  
        printProgressBar(ii + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)

    im_ani = animation.FuncAnimation(fig, ims, frames=len(images))
    # To save this second animation with some metadata, use the following command:
    im_ani.save(target, writer='imagemagick', fps=fps)
    
def m(source = "data/m.txt"):
    rcParams['figure.figsize'] = 20, 10
    label = []

    with open(source, 'r') as file:
        while True:
            line = file.readline()
            if line == '':
                break
            string = line.split(' ')
            label.append(string)
            
    t = []
    m = []
    for line in label:
        #print(line[2])
        #print(line[5])
        t.append(float(line[2]))
        m.append(float(line[5]))
    
    pltt.plot(t, m)
    
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()